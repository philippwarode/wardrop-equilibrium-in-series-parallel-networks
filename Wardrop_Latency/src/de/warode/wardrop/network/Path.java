package de.warode.wardrop.network;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Path implements Iterable<Arc> {

	private Node start;
	private Node end;
	private List<Arc> arcs;
	
	public Path(Node s, Node t, List<Arc> arcs) {
		this.start = s;
		this.end = t;
		this.arcs = new LinkedList<Arc>();
		this.arcs.addAll(arcs);
	}
	
	public Path(Arc a) {
		this.start = a.getStartNode();
		this.end = a.getEndNode();
		this.arcs = new LinkedList<Arc>();
		arcs.add(a);
	}
	
	public Node getStartNode() {
		return start;
	}
	
	public Node getEndNode() {
		return end;
	}

	@Override
	public Iterator<Arc> iterator() {
		return this.arcs.iterator();
	}
	
	public int getLength() {
		return this.arcs.size();
	}
	
	public Arc getArc(int i) throws ArrayIndexOutOfBoundsException {
		return this.arcs.get(i);
	}
	
	public static Path concat(Path p1, Path p2) {
		if (p1.end != p2.start) {
			throw new IllegalArgumentException("Path do not match!");
		}
		Path res = new Path(p1.start, p2.end, p1.arcs);
		res.arcs.addAll(p2.arcs);
		return res;
	}
	
	@Override
	public String toString() {
		//return this.arcs.toString();
		
		String s = "Path: " + arcs.get(0).getStartNode().getID();
		for (int i=1; i<this.getLength(); i++) {
			if (arcs.get(i).getStartNode() != arcs.get(i-1).getEndNode()) {
				return "Path: IS CORRUPT!";
			}
			s += " -> " + arcs.get(i).getStartNode().getID();
		}
		s += " -> " + arcs.get(this.getLength() - 1).getEndNode().getID();
		return s;
	}
}
