package de.warode.wardrop.network;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import de.warode.wardrop.function.AffineLinearFunction;
import de.warode.wardrop.function.Fraction;
import de.warode.wardrop.network.SeriesParallelComponent.ComponentType;

public class SerialComponent implements SeriesParallelComponent {
	
	private final SeriesParallelComponent[] serialComponents;
	private SeriesParallelNetwork owner;
	protected SeriesParallelComponent parent;
	private AffineLinearFunction latencyFunction;
	private AffineLinearFunction flowFunction;
	private Fraction demand = Fraction.ZERO;
	
	public SerialComponent(SeriesParallelNetwork owner, SeriesParallelComponent[] components) {
		this.owner = owner;
		for (SeriesParallelComponent tmpChild : components) {
			tmpChild.setParent(this);
		}
		this.serialComponents = components;
		for (int l=1; l<components.length; l++) {
			Node tmpNode = components[l].getStartNode();
			components[l].setStartNode(components[l-1].getEndNode());
			owner.removeNode(tmpNode);
		}
	}

	@Override
	public Iterator<SeriesParallelComponent> iterator() {
		return new Iterator<SeriesParallelComponent>() {
			int pos = 0;
			@Override
			public boolean hasNext() {
				return (pos < serialComponents.length);
			}

			@Override
			public SeriesParallelComponent next() throws ArrayIndexOutOfBoundsException {
				return serialComponents[pos++];
			}
		};
	}

	@Override
	public AffineLinearFunction getLatencyFunction() {
		return this.getLatencyFunction(false);
	}
	
	@Override
	public AffineLinearFunction getLatencyFunction(boolean recalculate) {
		//if recalculate is true, force the child components to recalculate as well
		if (recalculate) {
			for (SeriesParallelComponent c : this) {
				c.getLatencyFunction(true);
			}
		}
		if (this.latencyFunction == null || recalculate) this.latencyFunction = calculateLatencyFunction();
		//Force a recomputation of the flow function, if recalculate was set true
		if (recalculate) this.flowFunction = null;
		return this.latencyFunction;
	}

	private AffineLinearFunction calculateLatencyFunction() {
		AffineLinearFunction sum = this.serialComponents[0].getLatencyFunction();
		for (int i=1; i<this.size(); i++){
			sum = AffineLinearFunction.sum(sum, this.serialComponents[i].getLatencyFunction());
		}
		return sum;
	}

	@Override
	public Fraction latency(Fraction d) {
		return this.getLatencyFunction().evaluate(d);
	}

	@Override
	public int size() {
		if (serialComponents == null)
			return 0;
		return this.serialComponents.length;
	}

	@Override
	public void setStartNode(Node i) {
		this.serialComponents[0].setStartNode(i);
	}

	@Override
	public void setEndNode(Node j) {
		this.serialComponents[this.size()-1].setEndNode(j);
	}

	@Override
	public Node getStartNode() {
		return this.serialComponents[0].getStartNode();
	}

	@Override
	public Node getEndNode() {
		return this.serialComponents[this.size()-1].getEndNode();
	}
	
	@Override
	public String toString() {
		String s = ""; String sep="";
		for (SeriesParallelComponent c : this) {
			s += sep + "(" + c.toString() + ")";
			sep = " -> ";
		}
		return s;
	}

	@Override
	public AffineLinearFunction getFlowDistributionFunction(int i) {
		if (i<0 || i>=this.size()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		//Flow distribution behaves like the flow on one arc (=identity(Demand))
		return Arc.flowFunction;
	}
	
	@Override
	public AffineLinearFunction getFlowFunction() {
		//Compute the flow function if necessary
		if (this.flowFunction == null) {
			//If the parent is the network, return trivial function
			if (SeriesParallelComponent.ComponentType.getType(parent) ==ComponentType.NETWORK) {
				return owner.getFlowFunction();
			}
			//Find the ID of this component
			int id = -1;
			for (int i=0; i<parent.size(); i++) {
				if (parent.getSubComponent(i)==this) {
					id = i;
					break;
				}
			}
			this.flowFunction = parent.getFlowDistributionFunction(id).compose(parent.getFlowFunction());
		}
		return this.flowFunction;
	}

	@Override
	public void splitDemand(Fraction demand) {
		this.demand = demand;
		for(SeriesParallelComponent c : this) {
			c.splitDemand(demand);
		}
	}
	
	@Override
	public Fraction getRoutedDemand() {
		return this.demand;
	}

	@Override
	public Collection<Path> getPaths() {
		return collectPaths(0);
	}
	
	private Collection<Path> collectPaths(int i) {
		if (i==this.size() -1) {
			return this.serialComponents[i].getPaths();
		}
		Collection<Path> tmp = new LinkedList<Path>();
		Collection<Path> previousPaths = this.collectPaths(i+1);
		Collection<Path> currentPaths = this.serialComponents[i].getPaths();
		for (Path p : currentPaths) {
			for (Path q : previousPaths) {
				tmp.add(Path.concat(p, q));
			}
		}
		return tmp;
	}

	private int height = -1;
	@Override
	public int getHeight() {
		if (this.height != -1) return this.height;
		int tmpHeight = 1;
		for (SeriesParallelComponent c : this) {
			int h = c.getHeight();
			tmpHeight = tmpHeight >= h ? tmpHeight : h;  
		}
		this.height = tmpHeight;
		return tmpHeight;
	}

	private int length = -1;
	@Override
	public int getLength() {
		if (this.length != -1) return this.length;
		int tmpLength = 0;
		for (SeriesParallelComponent c : this) {
			tmpLength += c.getLength(); 
		}
		this.length = tmpLength;
		return tmpLength;
	}

	@Override
	public SeriesParallelComponent getSubComponent(int i) throws ArrayIndexOutOfBoundsException {
		if (i<0 || i>= this.serialComponents.length) throw new ArrayIndexOutOfBoundsException(i);
		return this.serialComponents[i];
	}

	@Override
	public SeriesParallelNetwork getOwner() {
		return this.owner;
	}
	
	@Override
	public SeriesParallelComponent getParent() {
		return this.parent;
	}
	
	@Override
	public void setParent(SeriesParallelComponent parent) {
		this.parent = parent;
	}


}
