package de.warode.wardrop.network;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

import de.warode.wardrop.function.*;

/**
 * A parallel component.<br>
 * <br>
 * For all methods from the interface <code>SeriesParallelCompoent</code>
 * see the description there.
 * 
 * @author Philipp Warode
 *
 */
public class ParallelComponent implements SeriesParallelComponent {

	private final SeriesParallelComponent[] parallelComponents;
	private AffineLinearFunction latencyFunction = null;
	private AffineLinearFunction[] flowDistribution = null;
	private AffineLinearFunction flowFunction = null;
	private Fraction demand = Fraction.ZERO;
	private SeriesParallelNetwork owner;
	private SeriesParallelComponent parent;
	
	public ParallelComponent(SeriesParallelNetwork owner, SeriesParallelComponent[] parallelComponents) {
		this.owner = owner;
		for (SeriesParallelComponent tmpChild : parallelComponents) {
			tmpChild.setParent(this);
		}
		SeriesParallelComponent[] sorted = parallelComponents;
		Arrays.sort(parallelComponents, new Comparator<SeriesParallelComponent>() {

			@Override
			public int compare(SeriesParallelComponent o1, SeriesParallelComponent o2) {
				return o1.getLatencyFunction().evaluate(Fraction.ZERO).compareTo(o2.getLatencyFunction().evaluate(Fraction.ZERO));
			}
			
		});
		this.parallelComponents = sorted;
		Node i = sorted[0].getStartNode();
		Node j = sorted[0].getEndNode();
		for (int l=1; l<sorted.length; l++) {
			Node tmpStart = sorted[l].getStartNode();
			Node tmpEnd = sorted[l].getEndNode();
			sorted[l].setStartNode(i);
			sorted[l].setEndNode(j);
			owner.removeNode(tmpStart); owner.removeNode(tmpEnd);
		}
	}

	@Override
	public Iterator<SeriesParallelComponent> iterator() {
		return new Iterator<SeriesParallelComponent>() {
			int pos = 0;
			@Override
			public SeriesParallelComponent next() throws ArrayIndexOutOfBoundsException {
				return parallelComponents[pos++];
			}
			
			@Override
			public boolean hasNext() {
				return (pos < parallelComponents.length);
			}
		};
	}

	@Override
	public AffineLinearFunction getLatencyFunction() {
		return this.getLatencyFunction(false);
	}
	
	public AffineLinearFunction getLatencyFunction(boolean recalculate) {
		//if recalculate is true, force the child components to recalculate as well
		if (recalculate) {
			for (SeriesParallelComponent c : this) {
				c.getLatencyFunction(true);
			}
		}
		
		//If latency function was not yet calculated, calculate it
		if (this.latencyFunction == null || recalculate) this.latencyFunction = computeEquilibrium();
		//Force a recomputation of the flow function, if recalculate was set true
		if (recalculate) this.flowFunction = null;
		return this.latencyFunction;
	}
	
	private AffineLinearFunction computeEquilibrium() {		
		int m = this.parallelComponents.length;
		/** M[i] == Number of breakpoints of function i **/
		int[] M = new int[m];
		/** Total number of all breakpoints **/
		int M_total = 2;
		for (int i=0; i<m; i++) {
			M[i] = this.parallelComponents[i].getLatencyFunction().getNumberOfBreakpoints() ;
			M_total += M[i];
		}
		
		/** number of the current breakpoint **/
		int i = 0;	
		/** number of currently used arcs - 1 **/
		int p = 0;
		/** index of the currently used function part of latency function i **/
		int[] j = new int[m];
		
		//Allocate Arrays 
		// Latency function coefficients
		Breakpoint[] tau = new Breakpoint[m*M_total];
		Fraction[] alpha = new Fraction[m*M_total];
		Fraction[] beta = new Fraction[m*M_total];
		// Flow function coefficients
		Fraction[][] gamma = new Fraction[m][m*M_total];
		Fraction[][] delta = new Fraction[m][m*M_total];
		
		//Set first breaktpoint to 0
		tau[0] = new Breakpoint(Fraction.ZERO, false);
		
		//Main loop
		while (tau[i].x.compareTo(Fraction.POSITIVE_INFINITY) < 0) {
			//Compute the harmonic mean
			Fraction tmpAlpha = Fraction.ZERO;
			int numOfZeroSlopes = 0;
			//Add all reciprocals
			for (int k=0; k<=p; k++) {
				Fraction tmpA = this.parallelComponents[k].getLatencyFunction().getSlope(j[k]);
				//Count the number of {a : a=0}
				if (tmpA.isZero()) {
					numOfZeroSlopes++;
				} else {
					tmpAlpha = tmpAlpha.add(tmpA.inv());
				}
			}
			if (numOfZeroSlopes == 0) {
				tmpAlpha = tmpAlpha.inv();
			} else {
				tmpAlpha = Fraction.ZERO;
			}
			alpha[i] = tmpAlpha;
			
			//Compute the betas
			if (i==0) {
				beta[i] = this.parallelComponents[0].getLatencyFunction().getConstant(0);
			} else {
				beta[i] = (alpha[i-1].sub(alpha[i])).mult(tau[i].x).add(beta[i-1]);
			}
			
			for (int k=0; k<m; k++) {
				//Compute the gammas
				if (k>p) {
					//Unused edge gets 0 Flow
					gamma[k][i] = Fraction.ZERO;
				} else {
					Fraction tmpA = this.parallelComponents[k].getLatencyFunction().getSlope(j[k]);
					if (tmpA.isZero()) {
						//Case of 0-slope (distribute flow equally)
						gamma[k][i] = new Fraction(1, numOfZeroSlopes);
					} else {
						gamma[k][i] = alpha[i].div(tmpA);
					}
				}
				
				//Compute the deltas
				if (i==0 || k>p) {
					delta[k][i] = Fraction.ZERO;
				} else {
					delta[k][i] = (gamma[k][i-1].sub(gamma[k][i])).mult(tau[i].x).add(delta[k][i-1]);
				}
			}
			
			//Compute d_1
			Fraction d_1 = Fraction.POSITIVE_INFINITY;
			int lambda = -1;
			// Compute the min
			for (int k=0; k<=p; k++) {
				Fraction tmpSigma = this.parallelComponents[k].getLatencyFunction().getBreakpoint(j[k]+1).x;
				if (!tmpSigma.isInfinite() && !gamma[k][i].isZero()) {
					Fraction tmpVal = (tmpSigma.sub(delta[k][i])).div(gamma[k][i]);
					if (tmpVal.compareTo(d_1)<0) {
						d_1 = tmpVal;
						lambda = k;
					}
				}
			}
			
			//Compute d_2
			Fraction d_2 = Fraction.POSITIVE_INFINITY;
			if (p < m-1 && !alpha[i].isZero()) {
				Fraction tmpB = this.parallelComponents[p+1].getLatencyFunction().getConstant(0);
				d_2 = (tmpB.sub(beta[i])).div(alpha[i]);
			}
			
			//Create new Breakpoint
			tau[i+1] = new Breakpoint(Fraction.min(d_1, d_2), false);
			//Compare d_1 and d_2
			int compare = d_1.compareTo(d_2);
			if (compare <= 0 && lambda != -1) {
				j[lambda] += 1;
			}
			
			if (compare >=0) {
				p=p+1;
			}
			
			i=i+1;			
		}
		
		//Shorten all Arrays
		// Latency function coefficients
		Breakpoint[] sTau = new Breakpoint[i+1];
		Fraction[] sAlpha = new Fraction[i];
		Fraction[] sBeta = new Fraction[i];
		// Flow function coefficients
		Fraction[][] sGamma = new Fraction[m][i];
		Fraction[][] sDelta = new Fraction[m][i];
		
		//Copy arrays
		sTau[0] = tau[0];
		for (int l=0; l<i; l++) {
			sTau[l+1] = tau[l+1];
			sAlpha[l] = alpha[l];
			sBeta[l] = beta[l];
			for (int k=0; k<m; k++) {
				sGamma[k][l] = gamma[k][l];
				sDelta[k][l] = delta[k][l];
			}
		}
		
		//Create and store flow distribution functions
		this.flowDistribution = new AffineLinearFunction[m];
		for (int k=0; k<m; k++) {
			this.flowDistribution[k] = new AffineLinearFunction(sGamma[k], sDelta[k], sTau);
		}
		
		//Create and store joint latency function
		this.latencyFunction = new AffineLinearFunction(sAlpha, sBeta, sTau);
		return this.latencyFunction;
	}
	

	@Override
	public Fraction latency(Fraction d) {
		return this.getLatencyFunction().evaluate(d);
	}
	

	@Override
	public int size() {
		if (this.parallelComponents == null)
			return 0;
		return this.parallelComponents.length;
	}
	
	public static ParallelComponent oneArcComponent(SeriesParallelNetwork owner, AffineLinearFunction latency) {
		Node i = owner.addNode();
		Node j = owner.addNode();
		Arc a = owner.addArc(i, j);
		a.latencyFunction = latency;
		SeriesParallelComponent[] arc = {a};
		ParallelComponent c = new ParallelComponent(owner, arc);
		a.setParent(c);
		return c;
	}

	@Override
	public void setStartNode(Node i) {
		for (SeriesParallelComponent c : this) {
			c.setStartNode(i);
		}
	}

	@Override
	public void setEndNode(Node j) {
		for (SeriesParallelComponent c : this) {
			c.setEndNode(j);
		}
	}

	@Override
	public Node getStartNode() {
		return this.parallelComponents[0].getStartNode();
	}

	@Override
	public Node getEndNode() {
		return this.parallelComponents[0].getEndNode();
	}
	
	@Override
	public String toString() {
		String s = ""; String sep="";
		for (SeriesParallelComponent c : this) {
			s += sep + "(" + c.toString() + ")";
			sep = " || ";
		}
		return s;
	}
	
	@Override
	public AffineLinearFunction getFlowDistributionFunction(int i) {
		if (this.latencyFunction == null) this.latencyFunction = computeEquilibrium();	
		return this.flowDistribution[i];
	}
	
	@Override
	public AffineLinearFunction getFlowFunction() {
		//Compute the flow function if necessary
		if (this.flowFunction == null) {
			//If the parent is the network, return trivial function
			if (SeriesParallelComponent.ComponentType.getType(parent) ==ComponentType.NETWORK) {
				return owner.getFlowFunction();
			}
			//Find the ID of this component
			int id = -1;
			for (int i=0; i<parent.size(); i++) {
				if (parent.getSubComponent(i)==this) {
					id = i;
					break;
				}
			}
			this.flowFunction = parent.getFlowDistributionFunction(id).compose(parent.getFlowFunction());
		}
		return this.flowFunction;
	}

	@Override
	public void splitDemand(Fraction demand) {
		this.demand = demand;
		if (this.latencyFunction == null) this.latencyFunction = this.getLatencyFunction();
		for (int i=0; i<this.size(); i++) {
			this.parallelComponents[i].splitDemand(this.flowDistribution[i].evaluate(demand));
		}
	}
	
	@Override
	public Fraction getRoutedDemand() {
		return this.demand;
	}

	@Override
	public Collection<Path> getPaths() {
		Collection<Path> tmp = new LinkedList<Path>();
		for (SeriesParallelComponent c : this) {
			tmp.addAll(c.getPaths());
		}
		return tmp;
	}

	private int heigth = -1;
	@Override
	public int getHeight() {
		if (this.heigth != -1) return this.heigth;
		int tmpHeigth = 0;
		for (SeriesParallelComponent c : this) {
			tmpHeigth += c.getHeight();
		}
		this.heigth = tmpHeigth;
		return tmpHeigth;
	}

	private int length = -1;
	@Override
	public int getLength() {
		if (this.length != -1) return this.length;
		int tmpLength = 1;
		for (SeriesParallelComponent c : this) {
			int l = c.getLength();
			tmpLength = tmpLength >= l ? tmpLength : l; 
		}
		this.length = tmpLength;
		return tmpLength;
	}

	@Override
	public SeriesParallelComponent getSubComponent(int i) throws ArrayIndexOutOfBoundsException {
		if (i<0 || i>= this.parallelComponents.length) throw new ArrayIndexOutOfBoundsException(i);
		return this.parallelComponents[i];
	}

	@Override
	public SeriesParallelNetwork getOwner() {
		return this.owner;
	}

	@Override
	public SeriesParallelComponent getParent() {
		return this.parent;
	}
	
	@Override
	public void setParent(SeriesParallelComponent parent) {
		this.parent = parent;
	}
	
}
