package de.warode.wardrop.network;

public enum EquilibriumType {
	WARDROP_EQUIBLIBRIUM, SYSTEM_OPTIMUM, UNSET;
	
	public String toString() {
		switch (this) {
		case WARDROP_EQUIBLIBRIUM:
			return "Wardop Equilibrium";
		case SYSTEM_OPTIMUM: 
			return "System Optimum";
		default:
			return "Unset";
		}
	}
}
