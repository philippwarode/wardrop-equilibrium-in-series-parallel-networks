package de.warode.wardrop.network;

import java.util.ArrayList;
import java.util.Collection;

import de.warode.wardrop.function.AffineLinearFunction;
import de.warode.wardrop.function.Fraction;

/**
 * Series-parallel Component interface.<br>
 * A series-parallel component is (1) a parallel component, (2) a serial component, or (3) a single arc.<br>
 * <br>
 * A series-parallel component is a collection of its subcomponent and is iterable. If the series-parallel component is
 * a single arc, then it only contains itself.<br>
 * <br>
 * Series-parallel components compute their joint-latency and the equilibrium flows depending on the demand.
 * 
 * @author Philipp Warode
 *
 */
public interface SeriesParallelComponent extends Iterable<SeriesParallelComponent> {

	/**
	 * Returns the joint latency function of this component in the equilibrium depending on the demand .
	 * If the latency was not yet computed, this function computes the latency and
	 * demand distribution functions for this component.<br>
	 * In case of a single arc, this returns the latency of the arc.
	 * @return the joint latency function of this component
	 */
	public AffineLinearFunction getLatencyFunction();
	/**
	 * Returns the joint latency function of this component in the equilibrium depending on the demand.
	 * If the parameter <code>recalculate</code> is set to <code>true</code>, the
	 * joint latency and flow functions will be recalculated even if they were
	 * computed before.<br>
	 * In case of a single arc, this returns the latency of the arc.
	 * @param recalculate
	 * @return the joint latency function of this component
	 */
	public AffineLinearFunction getLatencyFunction(boolean recalculate);
	/**
	 * This functions returns the share of flow routed through the subcomponent <code>i</code>
	 * in the equilibrium. If the joint latency was computed before, this returns
	 * the associated flow functions, otherwise the latency and flow function are computed.<br>
	 * In the case of a single arc, this returns the identity function
	 * <code>x(d) = d</code>
	 * @param i the index of the subcomponent
	 * @return the flow function of the i-th subcomponent depending on the demand d routed through this component
	 */
	public AffineLinearFunction getFlowDistributionFunction(int i);
	
	/**
	 * This function computes and returns the AffineLinearFunction giving the actual flow
	 * that is routed through this component depending on the overall demand in the
	 * whole network.
	 * @return the flow function depending on the overall demand of the network.
	 */
	public AffineLinearFunction getFlowFunction();
	
	/**
	 * Splits the demand <code>d</code> between the subcomponents and calls their
	 * <code>splitDemand</code> function recursively.
	 * @param d the demand routed through this component
	 */
	public void splitDemand(Fraction d);
	
	/**
	 * Computes the joint-latency of this component when an amount of <code>d</code>
	 * is routed through this component. 
	 * @param d the demand routed through this component
	 * @return the joint latency of this component
	 */
	public Fraction latency(Fraction d);
	/**
	 * @return the total number of subcomponents
	 */
	public int size();
	
	/**
	 * Connects this component with the start node <code>i</code>, i.e. the first node
	 * in this component is replaced by the node <code>i</code>
	 * @param i the designated start node
	 */
	public void setStartNode(Node i);
	/**
	 * Connects this component with the end node <code>j</code>, i.e. the last node
	 * in this component is replaced by the node <code>j</code>
	 * @param j the designated end node
	 */
	public void setEndNode(Node j);
	
	/**
	 * Returns the current start node, i.e. the first node of this component.
	 * @return the first node of this component.
	 */
	public Node getStartNode();
	/**
	 * Returns the current end node, i.e. the last node of this component.
	 * @return the last node of this component.
	 */
	public Node getEndNode();
	
	public Collection<Path> getPaths();
	
	/**
	 * Returns the visual height of this component.<br>
	 * A single arc has the height <code>h = 1</code>.<br>
	 * A serial component has the height <code>h = max {h(s) | s is subcomponent of this}</code><br>
	 * A parallel component has the height <code>h = h(1) + ... + h(m)</code> for the subcomponents
	 * <code>1, ... , m</code>.<br>
	 * <br>
	 * Used for the GUI.
	 * @return the visual height of this component
	 */
	public int getHeight();
	/**
	 * Returns the visual length of this component.<br>
	 * A single arc has the length <code>l = 1</code>.<br>
	 * A serial component has the height <code>l = l(1) + ... + l(m)</code> for the subcomponents
	 * <code>1, ... , m</code>.<br>
	 * A parallel component has the length <code>l = max {l(s) | s is subcomponent of this}</code><br>
	 * <br>
	 * Used for the GUI.
	 * @return the visual length of this component
	 */
	public int getLength();
	/**
	 * @param i the index of the subcomponent
	 * @return the i-th subcomponent
	 * @throws ArrayIndexOutOfBoundsException if <code>i&lt;0</code> or <code>i&gt;=this.size()</code>
	 */
	public SeriesParallelComponent getSubComponent(int i) throws ArrayIndexOutOfBoundsException;
	
	/**
	 * Returns the amount of flow currently routed through this component. This is the
	 * amount of demand that was specified in the last call of the <code>splitDemand</code>
	 * function (if <code>splitDemand</code> was not used, this returns 0).
	 * @return the current amount of flow in this component
	 */
	public Fraction getRoutedDemand();
	
	/**
	 * @return the owner network
	 */
	public SeriesParallelNetwork getOwner();
	
	public SeriesParallelComponent getParent();
	public void setParent(SeriesParallelComponent parent);
	
	public static SeriesParallelComponent[] hirachy(SeriesParallelComponent c) {
		ArrayList<SeriesParallelComponent> list = new ArrayList<SeriesParallelComponent>();
		list.add(c);
		while (c != null && ComponentType.getType(c) != ComponentType.NETWORK) {
			c = c.getParent();
			list.add(0,c);
		}
		
		return list.toArray(new SeriesParallelComponent[list.size()]);
	}
	
	public enum ComponentType {
		ARC, PARALLEL, SERIAL, NETWORK;
		public static ComponentType getType(SeriesParallelComponent c) {
			switch(c.getClass().getSimpleName()) {
			case "Arc":
				return ARC;
			case "SerialComponent":
				return SERIAL;
			case "ParallelComponent":
				return PARALLEL;
			default:
				return NETWORK;
			}
		}
		
		public String toString() {
			switch (this) {
			case ARC:
				return "Arc";
			case PARALLEL:
				return "Parallel Component";
			case SERIAL:
				return "Serial Component";
			case NETWORK:
				return "Series-parallel network";
			}
			return "Faulty ComponentType";
		}
	}
	
}
