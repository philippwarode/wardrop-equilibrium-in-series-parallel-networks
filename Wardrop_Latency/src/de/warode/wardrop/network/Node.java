package de.warode.wardrop.network;

import java.util.ArrayList;
import java.util.Collection;

public class Node {

	private Collection<Arc> incomingArcs, outgoingArcs;
	private final int id;
	private final Network owner;
	
	protected Node(int id, Network owner) {
		this.incomingArcs = new ArrayList<Arc>();
		this.outgoingArcs = new ArrayList<Arc>();
		this.id = id;
		this.owner = owner;
	}
	
	protected void addIncoming(Arc a) {
		this.incomingArcs.add(a);
	}
	
	protected void addOutgoing(Arc a) {
		this.outgoingArcs.add(a);
	}
	
	protected void removeIncoming(Arc a) {
		this.incomingArcs.remove(a);
	}
	
	protected void removeOutgoing(Arc a) {
		this.outgoingArcs.remove(a);
	}
	
	public int getID() {
		return id;
	}
	
	public Network getOwner() {
		return this.owner;
	}
	
	public boolean belongsTo(Network n) {
		return (n == owner);
	}
	
	@Override
	public String toString() {
		return "Node "+this.id+ " (Inc: "+this.incomingArcs.size() + " | Out: "+this.outgoingArcs.size() +")";
	}
	
}
