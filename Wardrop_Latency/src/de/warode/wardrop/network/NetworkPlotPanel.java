package de.warode.wardrop.network;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import de.warode.wardrop.NotYetImplementedException;

public class NetworkPlotPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final int EDGELENGTH = 150;
	private static final int EDGEHEIGHT = 50;
	private static final int NODESIZE = 30;
	private static final int NODESPACING = 5;
	
	private static final Color HIGHLIGHTCOLOR = new Color(180,255,180);
	
	private Network network;
	public int offsetX=-25;
	public int offsetY=-25;
	
	private SeriesParallelComponent selection = null;
	
	public NetworkPlotPanel() {
		this(null);
	}
	
	public NetworkPlotPanel(Network n) {
		super();
        setOpaque(false); // we don't paint all our bits
        setLayout(new BorderLayout());
		this.network = n;
	}
	
	public void setNetwork(Network n) {
		this.network = n;
	}
	
	public void select(SeriesParallelComponent component) {
		this.selection = component;
		this.repaint();
	}
	
	@Override
    public Dimension getPreferredSize() {
		if (this.network == null || this.network.getClass() != SeriesParallelNetwork.class) {
			return new Dimension(300,50);
		}
		SeriesParallelNetwork spNet = (SeriesParallelNetwork) this.network;
		
        return new Dimension(spNet.getLength() * EDGELENGTH + 2*NODESIZE, spNet.getHeight() * EDGEHEIGHT + 2*NODESPACING);
    }
	
	protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        
        
        Graphics2D g2d = (Graphics2D) g;
        Dimension size = getSize();
        
        if (this.network == null) {
        	g2d.drawString("No network loaded", 0 , size.height / 2);
        	return;
        }
        
  
        if (this.network.getClass() == SeriesParallelNetwork.class) {
        	plotSeriesParallelNetwork(g2d, size);
        	return;
        }
        
        throw new NotYetImplementedException("General Network can't be plotted yet");
	}

	private void plotSeriesParallelNetwork(Graphics2D g2d, Dimension size) {
		SeriesParallelNetwork spNet = (SeriesParallelNetwork) this.network;
		int tmpH = spNet.getHeight() * EDGEHEIGHT;
		int tmpW = spNet.getLength() * EDGELENGTH;
		g2d.drawRect(0 - offsetX - NODESIZE/2, 0 + NODESPACING/2 - offsetY - NODESIZE/2, NODESIZE, tmpH);
		g2d.drawString(Integer.toString(spNet.getStartNode().getID()), -offsetX, tmpH/2 - 5 - offsetY);
		g2d.drawRect(tmpW - offsetX - NODESIZE/2, 0 + NODESPACING/2 - offsetY - NODESIZE/2, NODESIZE, tmpH);
		g2d.drawString(Integer.toString(spNet.getEndNode().getID()), tmpW - offsetX, tmpH/2 - 5 - offsetY);
		drawComponent(g2d, 0,0, spNet.baseComponent);
	}
	
	private void drawComponent(Graphics2D g2d, int x, int y, SeriesParallelComponent comp) {
		//Highlight selection
		if (comp == this.selection) {
			g2d.setColor(HIGHLIGHTCOLOR);
			g2d.fillRect(x - offsetX + NODESIZE /2 + 1, y - offsetY - NODESIZE /2 + NODESPACING /2 ,
					comp.getLength() * EDGELENGTH - NODESIZE - 1, comp.getHeight() * EDGEHEIGHT);
			g2d.setColor(Color.BLACK);
		}
		
		if (comp.getClass() == ParallelComponent.class) {
			int n = comp.size();
			int h = comp.getHeight() * (EDGEHEIGHT + NODESPACING);
			for (int i = 0; i<n; i++) {
				drawComponent(g2d, x, y + i * h / n, comp.getSubComponent(i));
			}
		}
		if (comp.getClass() == Arc.class) {
			int x1 = x - offsetX + NODESIZE/2;
			int x2 = x - NODESIZE/2 - offsetX + EDGELENGTH;
			int tmpY = y - offsetY;
			g2d.drawLine(x1,tmpY, x2 , tmpY);
			g2d.drawLine(x2, tmpY, x2 - EDGEHEIGHT / 4, tmpY - EDGEHEIGHT /4);
			g2d.drawLine(x2, tmpY, x2 - EDGEHEIGHT / 4, tmpY + EDGEHEIGHT /4);
			String flow = "f=" + ((Arc) comp).flow.toString();
			g2d.setColor(Color.BLUE);
			g2d.drawString(flow, (x2+x1)/2 - 3*flow.length(), tmpY - 6);
			String latency = "c(x)=" + ((Arc) comp).latencyFunction.partToString(0, false);
			g2d.setColor(Color.BLACK);
			g2d.drawString(latency, (x2+x1)/2 - 3*latency.length(), tmpY + 14);
			//g2d.setColor(Color.BLACK);
		}
		if (comp.getClass() == SerialComponent.class) {
			int n = comp.size();
			int h = comp.getHeight() * EDGEHEIGHT;
			int pos = 0;
			for (int i = 0; i<n; i++) {
				SeriesParallelComponent c = comp.getSubComponent(i);
				drawComponent(g2d, x + pos * EDGELENGTH, y + (comp.getHeight() - c.getHeight()) * EDGEHEIGHT / 2, c);
				pos += c.getLength();
				//Draw End node
				if (i<n-1) {
					g2d.drawRect(x + EDGELENGTH * pos - offsetX - NODESIZE/2 , y - offsetY - NODESIZE/2 + NODESPACING/2,
							NODESIZE, h - NODESPACING * comp.getHeight());
					g2d.drawString(Integer.toString(c.getEndNode().getID()),
							x + EDGELENGTH * pos - offsetX, y + h /2 - offsetY - 5);
				}
			}
		}
	}

}
