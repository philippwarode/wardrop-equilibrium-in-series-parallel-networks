package de.warode.wardrop.network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import de.warode.wardrop.function.*;

public class NetworkLoader {
	
	private static Interval latencyDomain = new Interval(Fraction.ZERO, Fraction.POSITIVE_INFINITY);
	private static AffineLinearFunction zeroFunction = new AffineLinearFunction(Fraction.ZERO, Fraction.ZERO, latencyDomain);

	public static SeriesParallelNetwork loadSeriesParallelNetwork(String fileName) throws IOException, ParseException {
		return loadSeriesParallelNetwork(new File(fileName));
	}
	
	public static SeriesParallelNetwork loadSeriesParallelNetwork(File file) throws IOException, ParseException {
		
		ParseException parseException = null;
		IOException ioException = null;
		
		SeriesParallelNetwork net = new SeriesParallelNetwork();
		
		if (!file.exists() || !file.canRead() || !file.isFile()) {
			throw new IOException("File "+file.getName()+" does not exist or is not accessible.");
		}
		
		BufferedReader in = new BufferedReader(new FileReader(file));
		
		PseudoSerParComp currentComp = null;
		String line = null;
		int lineCounter = 0;
		try {
		while ((line = in.readLine()) != null) {
			lineCounter++;
			line = line.trim().toLowerCase();
			if (!line.startsWith("//") && line.length()!=0) {
				
				//No outer component, find the first component
				if (currentComp == null) {
					String typeString = line.substring(line.indexOf("<")+1, line.indexOf(">"));
					try {
						currentComp = new PseudoSerParComp(typeString, null);
					} catch (IllegalArgumentException e){
						throw new ParseException(e.getMessage(), lineCounter);
					}
				} else {
					//Close Components if end tag is detected
					SeriesParallelComponent tmpCreatedComp = null;
					switch (currentComp.type) {
					case PARALLEL:
						if (line.contains("</parallelcomponent>")) {
							if (currentComp.loadedChildren.size() == 0) {
								throw new ParseException("Parallel Component with no children", lineCounter);
							}
							tmpCreatedComp = new ParallelComponent(net, currentComp.loadedChildren.toArray(
											new SeriesParallelComponent[currentComp.loadedChildren.size()]));
						}
					case SERIAL:
						if (line.contains("</serialcomponent>")) {
							if (currentComp.loadedChildren.size() == 0) {
								throw new ParseException("Serial Component with no children", lineCounter);
							}
							tmpCreatedComp = new SerialComponent(net, currentComp.loadedChildren.toArray(
											new SeriesParallelComponent[currentComp.loadedChildren.size()]));
						}
					case ARC:
						if (line.contains("</arc>")) {
							tmpCreatedComp = ParallelComponent.oneArcComponent(net, currentComp.latencyFunction);
						}
					}
					//If an end-tag was detected before and a component was created
					//add this to the parent component or end parsing, if this was
					//the base component
					if (tmpCreatedComp != null) {
						if (currentComp.parent == null) {
							net.setBaseComponent(tmpCreatedComp);
							break;
						}
						currentComp = currentComp.parent;
						currentComp.loadedChildren.add(tmpCreatedComp);
					} else {
						//Detect Subcomponents or properties
						
						//Detect Properties of arcs
						if (currentComp.type == ComponentType.ARC) {
							if (!line.contains("<latency")) {
								throw new ParseException("Unknown Arc-Property: "+line, lineCounter);
							}
							ArrayList<Fraction> tmpA = findAttributeValues(line, "a");
							ArrayList<Fraction> tmpB = findAttributeValues(line, "b");
							ArrayList<Fraction> tmpDelta = findAttributeValues(line, "breaks");
							//First breakpoint has to be 0
							if (!tmpDelta.get(0).equals(Fraction.ZERO)) {
								tmpDelta.add(0, Fraction.ZERO);
							}
							//Create arrays from array list. If one length exceeds the other, ignore all additional paramets
							int numFctParts = Math.min(Math.min(tmpA.size(), tmpB.size()), tmpDelta.size());
							Fraction[] coeffA = new Fraction[numFctParts];
							Fraction[] coeffB = new Fraction[numFctParts];
							Breakpoint[] delta = new Breakpoint[numFctParts+1];
							for (int i=0; i<numFctParts; i++) {
								coeffA[i] = tmpA.get(i);
								coeffB[i] = tmpB.get(i);
								delta[i] = new Breakpoint(tmpDelta.get(i), i!=0);
							}
							//add the domain end manually
							delta[numFctParts] = new Breakpoint(Fraction.POSITIVE_INFINITY);
							currentComp.latencyFunction = new AffineLinearFunction(coeffA, coeffB, delta);
						} else {
							//DetectSubComponents, if currentComp != ARC
							String typeString = line.substring(line.indexOf("<")+1, line.indexOf(">"));
							try {
								PseudoSerParComp tmp = new PseudoSerParComp(typeString, null);
								tmp.parent = currentComp;
								currentComp = tmp;
							} catch (IllegalArgumentException e){
								throw new ParseException(e.getMessage(), lineCounter);
							}
						}
					}				
				}
				
			}
		}
		} catch (ParseException e) {
			parseException =  e;
		} catch (IOException e) {
			ioException = e;
		} catch (Exception e) {
			parseException = new ParseException("Invalid line:\n"+line, lineCounter);
			e.printStackTrace();
		} finally {
			in.close();
			if (parseException != null) {
				throw parseException;
			}
			if (ioException != null) {
				throw ioException;
			}
		}
		
		if (net.baseComponent == null) {
			throw new ParseException("Network contains no component!", lineCounter);
		}
		
		
		return net;
	}
	
	private static ArrayList<Fraction> findAttributeValues(String line, String attrName) throws ParseException {
		//Find offsets b
		ArrayList<Fraction> list = new ArrayList<Fraction>();
		int posOfAttr = line.indexOf(attrName+"=\""); 
		if (posOfAttr >= 0) {
			int endOfAttr = line.indexOf("\"", posOfAttr+2+attrName.length());
			endOfAttr = endOfAttr < 0 ? line.length() - 1 : endOfAttr;
			String content = line.substring(posOfAttr+2+attrName.length(), endOfAttr);
			String[] tmpParts = content.split(",");
			for (String s : tmpParts) {
				list.add(Fraction.parse(s));
			}
		} else {
			// If the b parameter is not given, assume b="0"
			list.add(Fraction.ZERO);
		}
		return list;
	}
	
	private static class PseudoSerParComp {
		
		private PseudoSerParComp parent;
		private ArrayList<SeriesParallelComponent> loadedChildren = new ArrayList<SeriesParallelComponent>();
		private ComponentType type;
		
		private AffineLinearFunction latencyFunction = NetworkLoader.zeroFunction;
		
		private PseudoSerParComp(ComponentType t, PseudoSerParComp parent) {
			this.type = t;
			this.parent = parent;
		}
		
		private PseudoSerParComp(String typeName, PseudoSerParComp parent) {
			this.type = ComponentType.fromString(typeName);
			this.parent = parent;
		}
		
	}
	
	private enum ComponentType {
		ARC, SERIAL, PARALLEL;
		
		private static ComponentType fromString(String s) {
			s = s.toLowerCase().trim();
			switch (s) {
			case "arc":
				return ComponentType.ARC;
			case "serialcomponent":
				return ComponentType.SERIAL;
			case "parallelcomponent":
				return ComponentType.PARALLEL;
			default:
				throw new IllegalArgumentException("Unknown Component Type: "+s);
			}
		}
		
	}
	
}
