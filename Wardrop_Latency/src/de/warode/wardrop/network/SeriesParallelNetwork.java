package de.warode.wardrop.network;

import java.util.Collection;
import java.util.Iterator;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import de.warode.wardrop.function.AffineLinearFunction;
import de.warode.wardrop.function.Fraction;

public class SeriesParallelNetwork extends Network implements SeriesParallelComponent{
	
	SeriesParallelComponent baseComponent;
	
	private Fraction demand = Fraction.ZERO;
	private EquilibriumType currentEquilibriumType = EquilibriumType.UNSET;
	
	public void setBaseComponent(SeriesParallelComponent base) {
		this.baseComponent = base;
		base.setParent(this);
	}


	@Override
	public Iterator<SeriesParallelComponent> iterator() {
		return baseComponent.iterator();
	}



	@Override
	public AffineLinearFunction getLatencyFunction() {
		return baseComponent.getLatencyFunction();
	}
	
	@Override
	public AffineLinearFunction getLatencyFunction(boolean recalculate) {
		return baseComponent.getLatencyFunction(recalculate);
	}


	@Override
	public Fraction latency(Fraction d) {
		//In case the current Equilibrium-Type is not WARDROP
		//which means not the real latency functions are used, throw Exception 
		if (this.currentEquilibriumType != EquilibriumType.WARDROP_EQUIBLIBRIUM) {
			//System.out.println("WARNING! Latency used while Equlibirum Type was NOT Wardrop. Latency may be marginal cost!");
			Fraction tmpLatency = Fraction.ZERO;
			Collection<Arc> allArcs = this.arcs.values();
			for (Arc a : allArcs) {
				tmpLatency = tmpLatency.add((a.latencyFunction.evaluate(a.flow)).mult(a.flow));
			}
			return tmpLatency;
		}
		return baseComponent.latency(d);
	}

	@Override
	public int size() {
		return baseComponent.size();
	}


	@Override
	public void setStartNode(Node i) {
		this.baseComponent.setStartNode(i);
	}


	@Override
	public void setEndNode(Node j) {
		this.baseComponent.setEndNode(j);
	}


	@Override
	public Node getStartNode() {
		return this.baseComponent.getStartNode();
	}


	@Override
	public Node getEndNode() {
		return this.baseComponent.getEndNode();
	}
	
	@Override
	public String toString() {
		String s = "SERIES PARALLEL "+super.toString();
		s+="\nStructure:\n"+baseComponent.toString();
		return s;
	}


	@Override
	public AffineLinearFunction getFlowDistributionFunction(int i) {
		return baseComponent.getFlowDistributionFunction(i);
	}


	@Override
	public void splitDemand(Fraction demand) throws IllegalArgumentException {
		
		this.demand = demand;
		this.baseComponent.splitDemand(demand);
		
	}
	
	public Fraction getRoutedDemand() {
		return this.demand;
	}
		
	public String getEquilibrium(Fraction demand, EquilibriumType t) throws IllegalArgumentException {
		//Catch UNSET Equilibrium type
		if (t == EquilibriumType.UNSET) {
			throw new IllegalArgumentException("Illegal Equilibrium type: "+t);
		}
		//if equilibrium type changed, force recalculation
		if (t != currentEquilibriumType) {
			this.currentEquilibriumType = t;
			this.baseComponent.getLatencyFunction(true);
		}
		//calculate Equilibrium flow
		this.splitDemand(demand);
		
		String s = this.currentEquilibriumType + "\n";
		s += "Routed demand: "+this.demand+"\n\n";
		s += "Arc-Flows: \n";
		s += "Edge \t Flow \t Latency \n";
		Collection<Arc> allArcs = this.arcs.values();
		for (Arc a : allArcs) {
			s += a.toString() + " \t "+a.flow+" \t +"+a.latency(a.flow)+"\n";
		}
		s += "\n Total latency: "+this.latency(demand);
		s += "\n\nPath-Flows:\n";
		s += "Path \t Flow \t Latency\n";
		Collection<Path> paths = this.getPaths();
		for (Path p : paths) {
			Fraction tmpFlow = Fraction.POSITIVE_INFINITY;
			Fraction tmpLatency = Fraction.ZERO;
			//TODO: this is not a real path decomposition
			for (Arc a : p) {
				tmpFlow = tmpFlow.compareTo(a.flow) > 0 ? a.flow : tmpFlow;
				tmpLatency = tmpLatency.add(a.latencyFunction.evaluate(a.flow));
			}
			
			s += p + "\t" + tmpFlow + "\t" + tmpLatency + "\n";
		}
		return s;
	}


	@Override
	public Collection<Path> getPaths() {
		return this.baseComponent.getPaths();
	}


	@Override
	public int getHeight() {
		return baseComponent.getHeight();
	}


	@Override
	public int getLength() {
		return baseComponent.getLength();
	}


	@Override
	public SeriesParallelComponent getSubComponent(int i) throws ArrayIndexOutOfBoundsException {
		return this.baseComponent.getSubComponent(i);
	}
	
	public TreeModel getTreeModel() {
		TreeModel tree = new TreeModel() {

			@Override
			public Object getRoot() {
				return baseComponent;
			}

			@Override
			public Object getChild(Object parent, int index) {
				return ((SeriesParallelComponent) parent).getSubComponent(index);
			}

			@Override
			public int getChildCount(Object parent) {
				return ((SeriesParallelComponent) parent).size();
			}

			@Override
			public boolean isLeaf(Object node) {
				return (node.getClass() == Arc.class);
			}

			@Override
			public void valueForPathChanged(TreePath path, Object newValue) {
				
			}

			@Override
			public int getIndexOfChild(Object parent, Object child) {
				SeriesParallelComponent c = (SeriesParallelComponent) parent;
				for (int i=0; i<c.size(); i++) {
					if (child == c.getSubComponent(i)) {
						return i;
					}
				}
				return -1;
			}

			@Override
			public void addTreeModelListener(TreeModelListener l) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void removeTreeModelListener(TreeModelListener l) {
				// TODO Auto-generated method stub
				
			}
			
		};
		return tree;
	}


	@Override
	public SeriesParallelNetwork getOwner() {
		return this;
	}
	
	public EquilibriumType getCurrentEquilibriumType() {
		return this.currentEquilibriumType;
	}
	
	@Override
	public SeriesParallelComponent getParent() {
		return null;
	}

	@Override
	public void setParent(SeriesParallelComponent parent) {
		//Parent can not be set in a network
	}


	@Override
	public AffineLinearFunction getFlowFunction() {
		return Arc.flowFunction;
	}

	
}
