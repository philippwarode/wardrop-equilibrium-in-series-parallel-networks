package de.warode.wardrop.network;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import de.warode.wardrop.function.*;

public class Arc implements SeriesParallelComponent {

	private static Function zeroFunction = Function.constantFunction(0, new Interval(Fraction.ZERO, Fraction.POSITIVE_INFINITY));
	
	protected static AffineLinearFunction flowFunction = 
			new AffineLinearFunction(new Fraction(1), Fraction.ZERO, new Interval(Fraction.ZERO, Fraction.POSITIVE_INFINITY, false, true));
	
	private Node start, end;
	
	private final int id;
	
	private SeriesParallelComponent parent;
	
	public Fraction flow = Fraction.ZERO;
	public Function latencyFunction = Arc.zeroFunction;
	
	public Arc(Node i, Node j, int id) {
		this.start = i;
		this.end = j;
		j.addIncoming(this);
		i.addOutgoing(this);
		this.id = id;
	}
	
	public Node startNode() {
		return this.start;
	}
	
	public Node endNode() {
		return this.end;
	}
	
	public int getID() {
		return this.id;
	}

	protected void destroy() {
		this.start.removeOutgoing(this);
		this.end.removeIncoming(this);
	}
	
	//Functions of the interface SeriesParallelComponent
	
	@Override
	public AffineLinearFunction getLatencyFunction() {
		return this.getLatencyFunction(false);
	}
	
	@Override
	public AffineLinearFunction getLatencyFunction(boolean recalculate) {
		if (this.latencyFunction.getClass() != AffineLinearFunction.class) throw new IllegalAccessError("Arc has not affine linear function as latency function");
		AffineLinearFunction l = (AffineLinearFunction) this.latencyFunction;
		Network owner = this.start.getOwner();
		//If owner is a Series-Parallel-Network, return marginal cost
		//if the SO is desired
		if (owner.getClass() == SeriesParallelNetwork.class) {
			SeriesParallelNetwork o = (SeriesParallelNetwork) owner;
			if (o.getCurrentEquilibriumType() == EquilibriumType.SYSTEM_OPTIMUM) {
				if (l.getNumberOfBreakpoints() > 0) {
					System.out.println("System optimum incorrect since arcs are not differentiable.");
				}
				Fraction[] tmpA = new Fraction[l.getNumberOfBreakpoints()+1];
				Fraction[] tmpB = new Fraction[l.getNumberOfBreakpoints()+1];
				Breakpoint[] tmpD = new Breakpoint[l.getNumberOfBreakpoints()+2];
				tmpD[0] = new Breakpoint(Fraction.ZERO, false);
				Fraction two = new Fraction(2);
				for (int i=0; i<l.getNumberOfBreakpoints()+1; i++) {
					tmpA[i] = two.mult(l.getSlope(i));
					tmpB[i] = l.getConstant(i); 
					tmpD[i+1] = l.getBreakpoint(i+1); 
				}
				AffineLinearFunction tmpMarg = new AffineLinearFunction("marg("+l.getName()+")", tmpA, tmpB, tmpD);
				return tmpMarg;
			}
		}
		
		return l;
	}

	@Override
	public Fraction latency(Fraction d) {
		return this.getLatencyFunction().evaluate(d);
	}

	@Override
	public Iterator<SeriesParallelComponent> iterator() {
		return new Iterator<SeriesParallelComponent>() {
			boolean nextUsed = false;
			@Override
			public boolean hasNext() {
				return !nextUsed;
			}

			@Override
			public SeriesParallelComponent next() {
				if (nextUsed) throw new ArrayIndexOutOfBoundsException();
				nextUsed = true;
				return Arc.this;
			}
		};
	}

	@Override
	public int size() {
		return 1;
	}

	@Override
	public void setStartNode(Node i) {
		this.start.removeOutgoing(this);
		i.addOutgoing(this);
		this.start = i;
	}

	@Override
	public void setEndNode(Node j) {
		this.end.removeIncoming(this);
		j.addIncoming(this);
		this.end = j;
	}

	@Override
	public Node getStartNode() {
		return this.start;
	}

	@Override
	public Node getEndNode() {
		return this.end;
	}
	
	@Override
	public String toString() {
		return this.start.getID() + " -> " + this.end.getID();
	}

	@Override
	public AffineLinearFunction getFlowDistributionFunction(int i) {
		if (i!=0)
			throw new ArrayIndexOutOfBoundsException(i);
		return Arc.flowFunction;
	}

	@Override
	public void splitDemand(Fraction demand) {
		this.flow = demand;
	}
	
	@Override
	public Fraction getRoutedDemand() {
		return this.flow;
	}

	@Override
	public Collection<Path> getPaths() {
		Collection<Path> tmp = new LinkedList<Path>();
		tmp.add(new Path(this));
		return tmp;
	}

	@Override
	public int getHeight() {
		return 1;
	}

	@Override
	public int getLength() {
		return 1;
	}

	@Override
	public SeriesParallelComponent getSubComponent(int i) throws ArrayIndexOutOfBoundsException {
		if (i!=0) throw new ArrayIndexOutOfBoundsException(i);
		return this;
	}

	@Override
	public SeriesParallelNetwork getOwner() {
		return (SeriesParallelNetwork) this.start.getOwner();
	}
	
	@Override
	public SeriesParallelComponent getParent() {
		return this.parent;
	}

	@Override
	public void setParent(SeriesParallelComponent parent) {
		this.parent = parent;
	}

	@Override
	public AffineLinearFunction getFlowFunction() {
		return this.parent.getFlowFunction();
	}

	
}
