package de.warode.wardrop.network;

import java.util.Collection;
import java.util.HashMap;

public abstract class Network {
	
	protected HashMap<Integer, Arc> arcs;
	protected HashMap<Integer, Node> nodes;
	
	private int nextNodeID=0;
	private int nextArcID=0;
	
	public Network() {
		this.arcs = new HashMap<Integer, Arc>();
		this.nodes = new HashMap<Integer, Node>();
	}
	
	public Node addNode() {
		Node tmpNode = new Node(nextNodeID, this);
		nodes.put(nextNodeID, tmpNode);
		nextNodeID++;
		return tmpNode;
	}
	
	public Arc addArc(Node i, Node j) {
		if (!(i.belongsTo(this) && j.belongsTo(this)))
			throw new IllegalArgumentException("The nodes do not belong to this network!");
		Arc tmpArc = new Arc(i,j, nextArcID);
		arcs.put(nextArcID, tmpArc);
		nextArcID++;
		return tmpArc;
	}
	
	public void removeNode(int id) {
		nodes.remove(id);
	}
	public void removeNode(Node n) {
		if (!n.belongsTo(this))
			throw new IllegalArgumentException("The node does not belong to this network!");
		removeNode(n.getID());
	}
	
	public void removeArc(int id) {
		Arc tmp = arcs.remove(id);
		if (tmp!=null) {
			tmp.destroy();
		}
	}
	
	public Node getNode(int id) throws IndexOutOfBoundsException {
		return nodes.get(id);
	}
	
	public Arc getArc(int id) throws IndexOutOfBoundsException {
		return arcs.get(id);
	}
	
	public String toString() {
		String s = "NETWORK\n";
		s += "Nodes:";
		Collection<Node> nodes = this.nodes.values();
		for (Node n : nodes) {
			s+= "\n" + n.toString();
		}
		s += "\nArcs:";
		Collection<Arc> arcs = this.arcs.values();
		for (Arc a : arcs) {
			s+= "\n" + a.toString();
		}
		return s;
	}
	
}
