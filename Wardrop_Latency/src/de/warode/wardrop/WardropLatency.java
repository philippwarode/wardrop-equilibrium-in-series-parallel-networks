package de.warode.wardrop;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import de.warode.wardrop.function.*;
import de.warode.wardrop.network.*;

public class WardropLatency {

	
	public static void createFunctionPlot(Function func) {
        JFrame f = new JFrame("Function Test Plot");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        FunctionPlotPanel panel = new FunctionPlotPanel(func);
        panel.drawColor = Color.BLUE;
        f.getContentPane().add(panel, BorderLayout.CENTER);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
	
	public static void createNetworkPlot(Network n) {
        JFrame f = new JFrame("Network Test Plot");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        NetworkPlotPanel panel = new NetworkPlotPanel(n);
        f.getContentPane().add(panel, BorderLayout.CENTER);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
	
	public static void main(String[] args) {
		
		SeriesParallelNetwork net = new SeriesParallelNetwork();
		
		Interval domain = new Interval(Fraction.ZERO, Fraction.POSITIVE_INFINITY, false, true);
		
		ParallelComponent u1,u2,u3, l1,l2,l3;
		//Upper Component
		u1 = ParallelComponent.oneArcComponent(net,
				new AffineLinearFunction("c_1", 1, 0, domain));
		u2 = ParallelComponent.oneArcComponent(net,
				new AffineLinearFunction("c_2", Fraction.ZERO, new Fraction(1,3), domain));
		u3 = ParallelComponent.oneArcComponent(net,
				new AffineLinearFunction("c_3", 1, 0, domain));
		
		ParallelComponent[] arrUPar = {u2, u3};
		ParallelComponent uPar = new ParallelComponent(net, arrUPar);
		
		System.out.println(uPar.getLatencyFunction());	
		for (int i=0; i<uPar.size(); i++) {
			System.out.println("Fluss von Komponente "+i+" von uPar:");
			System.out.println(uPar.getFlowDistributionFunction(i));
		}
		
		/*if (true) {
			return;
		}*/
		
		
		ParallelComponent[] arrUp = {u1, uPar};
		SerialComponent upper = new SerialComponent(net, arrUp);
		
		System.out.println(upper.getLatencyFunction());
		
		
		//Lower Component
		l1 = ParallelComponent.oneArcComponent(net,
				new AffineLinearFunction(1, 0, domain));
		l2 = ParallelComponent.oneArcComponent(net,
				new AffineLinearFunction(Fraction.ZERO, new Fraction(1,2), domain));
		l3 = ParallelComponent.oneArcComponent(net,
				new AffineLinearFunction(1, 0, domain));
		
		ParallelComponent[] arrLPar = {l2, l3};
		ParallelComponent lPar = new ParallelComponent(net, arrLPar);
		
		//System.out.println(uPar.getLatencyFunction());		
		
		ParallelComponent[] arrLo = {l1, lPar};
		SerialComponent lower = new SerialComponent(net, arrLo);
		
		System.out.println(lower.getLatencyFunction());
		
		//Total Network
		
		SeriesParallelComponent[] baseArr = {upper, lower};
		ParallelComponent base = new ParallelComponent(net, baseArr);
		
		net.setBaseComponent(base);
		
		System.out.println(net);
		System.out.println(net.getLatencyFunction());
		
		for (int i=0; i<net.size(); i++) {
			System.out.println("\n Flow of Component: "+i);
			System.out.println(net.getFlowDistributionFunction(i));
		}
		
		System.out.println(net.getEquilibrium(new Fraction(1), EquilibriumType.WARDROP_EQUIBLIBRIUM));
				
		System.out.println(net.getEquilibrium(new Fraction(1), EquilibriumType.SYSTEM_OPTIMUM));
		
		Runnable guiFunc = new Runnable() {
			private Function f = net.getLatencyFunction();
			
            @Override
            public void run() {
                createFunctionPlot(f);
            }
        };
        
        System.out.println(net.getLatencyFunction());
        //GUI must start on EventDispatchThread:
        SwingUtilities.invokeLater(guiFunc);
        
        Runnable guiNetwork = new Runnable() {
			private Network n = net;
			
            @Override
            public void run() {
                createNetworkPlot(n);
            }
        };
        
        //GUI must start on EventDispatchThread:
        SwingUtilities.invokeLater(guiNetwork);
		
	}

}
