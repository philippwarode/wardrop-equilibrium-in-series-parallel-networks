package de.warode.wardrop.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import de.warode.wardrop.function.AffineLinearFunction;
import de.warode.wardrop.function.Fraction;
import de.warode.wardrop.function.PolynomialFunction;
import de.warode.wardrop.network.*;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel mainPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private NetworkPlotPanel pnlNetwork;
	private JScrollPane scrNetwork;
	private JTree networkTree;
	private ComponentInfoPanel panelCompInfo;
	private JTextField txtDemand;
	
	private SeriesParallelNetwork network = null;
	
	private JButton btnWardrop;
	private JButton btnSO;
	
	public MainWindow() {
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		mainPanel.setLayout(new BorderLayout());
		
		//Setup Buttons
		buttonPanel.setLayout(new GridLayout(1,3));
		JButton tmpBtn = new JButton("Load Network");
		tmpBtn.addActionListener(new ButtonListener("load"));
		buttonPanel.add(tmpBtn);
		btnWardrop = new JButton("Wardrop Equilibrium");
		btnWardrop.addActionListener(new ButtonListener("wardrop"));
		btnWardrop.setEnabled(false);
		buttonPanel.add(btnWardrop);
		btnSO = new JButton("Social Optimum");
		btnSO.addActionListener(new ButtonListener("so"));
		btnSO.setEnabled(false);
		buttonPanel.add(btnSO);
		txtDemand = new JTextField("1");
		buttonPanel.add(txtDemand);		
		
		pnlNetwork = new NetworkPlotPanel();
		
		networkTree = new JTree();
		networkTree.setVisible(false);
		networkTree.setPreferredSize(new Dimension(300, 500));
		networkTree.addTreeSelectionListener(new NetworkTreeSelectionListener());
		
		panelCompInfo = new ComponentInfoPanel();
		
		scrNetwork = new JScrollPane(pnlNetwork);
		
		mainPanel.add(buttonPanel, BorderLayout.NORTH);
		mainPanel.add(scrNetwork, BorderLayout.CENTER);
		mainPanel.add(networkTree, BorderLayout.WEST);
		mainPanel.add(panelCompInfo, BorderLayout.EAST);
		
		this.getContentPane().add ( mainPanel ) ;
	}
	
	private void setNetwork(SeriesParallelNetwork n) {
		btnWardrop.setEnabled(n != null);
		btnSO.setEnabled(n != null);
		
		this.network = n;
		this.pnlNetwork.setNetwork(this.network);
		this.pnlNetwork.repaint();
		
		this.scrNetwork.validate();
		
		this.networkTree.setVisible(n != null);
		if (n!=null) this.networkTree.setModel(network.getTreeModel());
		
	}
	
	private class NetworkTreeSelectionListener implements TreeSelectionListener {

		@Override
		public void valueChanged(TreeSelectionEvent e) {
			try {
				SeriesParallelComponent selection = (SeriesParallelComponent) e.getPath().getLastPathComponent();
				MainWindow.this.pnlNetwork.select(selection);
				MainWindow.this.panelCompInfo.select(selection);
			} catch (ClassCastException ex) {
				
			}
		}
		
	}
	
	private class ButtonListener implements ActionListener {

		private String button;
		
		private ButtonListener(String b) {
			this.button = b;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			switch (this.button) {
			case "load":
				loadBtn(); break;
			case "wardrop":
				wardropBtn(); break;
			case "so":
				soBtn(); break;
			}
		}
		
		private void soBtn() {
			Fraction d;
			try {
				d = Fraction.parse(txtDemand.getText());
				MainWindow.this.network.getEquilibrium(d , EquilibriumType.SYSTEM_OPTIMUM);
				MainWindow.this.pnlNetwork.repaint();
				MainWindow.this.panelCompInfo.validate();
			} catch (ParseException e) {
				System.out.println(e);
			}
		}

		private void wardropBtn() {
			Fraction d;
			try {
				d = Fraction.parse(txtDemand.getText());
				MainWindow.this.network.getEquilibrium(d , EquilibriumType.WARDROP_EQUIBLIBRIUM);
				MainWindow.this.pnlNetwork.repaint();
				MainWindow.this.panelCompInfo.validate();
			} catch (ParseException e) {
				System.out.println(e);
			}
		}

		private void loadBtn() {
			SeriesParallelNetwork n = null;
			
			try {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("./files/"));
				
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					n = NetworkLoader.loadSeriesParallelNetwork(chooser.getSelectedFile());
				} else {
					n = MainWindow.this.network;
				}
				
			} catch (Exception ex) {
				System.out.println("Error while loading network:\n");
				System.out.println(ex);
			} finally {
				MainWindow.this.setNetwork(n);
			}
		}
		
	}

}
