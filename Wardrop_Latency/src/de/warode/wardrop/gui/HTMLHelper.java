package de.warode.wardrop.gui;

import de.warode.wardrop.function.*;

public class HTMLHelper {

	public static String fractionVariable(String varName, Fraction value) {
		if (value.isInteger()) {
			return varName + " = " + value.getNumerator();
		}
		String s = "<table><tr><td rowspan=2>"+varName+" = </td>";
		s += "<td style=\"border-bottom:1px solid #000000;\">"+value.getNumerator()+"</td></tr>";
		s += "<tr><td>"+value.getDenominator()+"</td></tr></table>";
		return s;
	}
	
	public static String function(Function f, String name) {
		int n = f.getNumberOfBreakpoints() + 1;
		String s = "<table><tr><td rowspan="+n+" style=\"border-right:1px solid #000000;\">"+ name + "(x) = </td>";
		s += "<td>" + f.partToString(0, false) + "</td><td>" + domainPartString(f.getBreakpoint(0), f.getBreakpoint(1)) + "</td></tr>";
		for (int i=2; i<n+1; i++) {
			s += "<tr><td>" + f.partToString(i-1, false) + "</td><td>" + domainPartString(f.getBreakpoint(i-1), f.getBreakpoint(i)) + "</td></tr>";
		}
		s+="</table>";
		return s;
	}
	
	public static String function(Function f) {
		return function(f, f.getName());
	}
	
	private static String domainPartString(Breakpoint l, Breakpoint r) {
		String s = "";
		if (!l.x.equals(Fraction.NEGATIVE_INFINITY)) {
			s = l.x.toString();
			if (l.left) {
				s += "&lt;";
			} else {
				s += "&le;";
			}
		}
		s += " x ";
		if (!r.x.equals(Fraction.POSITIVE_INFINITY)) {
			if (r.left) {
				s+= "&le; ";
			} else {
				s+= "&lt; ";
			}
			s += r.x.toString();
		}
		return s;
	}
	
	public static String html(String[] content) {
		String html ="<html><body>";
		for (String s : content) {
			html += s;
		}
		html += "</body></html>";
		return html;
	}
	
	public static String html(String content) {
		String[] tmp = {content};
		return html(tmp);
	}
	
}
