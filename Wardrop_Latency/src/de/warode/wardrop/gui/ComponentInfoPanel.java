package de.warode.wardrop.gui;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.warode.wardrop.function.*;
import de.warode.wardrop.network.*;

public class ComponentInfoPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private SeriesParallelComponent component = null;
	private JLabel lblCaption = new JLabel(HTMLHelper.html("<big>Component Info</big>"));
	private JLabel lblName = new JLabel("no component selected");
	private JLabel lblCaption1 = new JLabel(HTMLHelper.html("<b>Flow/Latency</b>"));
	private JLabel lblFlow = new JLabel("f = --");
	private JLabel lblLatency = new JLabel("l(x) = --");
	private JLabel lblRealLatency = new JLabel("l(x) = --");
	private JLabel lblCaption2 = new JLabel(HTMLHelper.html("<b>Latency (depending on the flow through this component)</b>"));
	private JLabel lblLatencyFct = new JLabel("l(x) = --");
	private FunctionPlotPanel panelLatency = new FunctionPlotPanel();
	private JLabel lblFlowCaption = new JLabel(HTMLHelper.html("<b>Flow Function (depending on overall demand d):</b>"));
	private JLabel lblFlowFct = new JLabel("x(d) = --");
	
	public ComponentInfoPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setPreferredSize(new Dimension(450, 700));
		Component[] tmp = {lblCaption, lblName, lblCaption1, lblFlow, lblRealLatency,
				lblLatency, lblCaption2, lblLatencyFct, panelLatency, lblFlowCaption, lblFlowFct};
		lblRealLatency.setVisible(false);
		for (Component c : tmp) {
			this.add(c);
		}
	}
	
	public void select(SeriesParallelComponent c) {
		this.component = c;
		if (component == null) return;
		lblName.setText(SeriesParallelComponent.ComponentType.getType(component).toString() + ": " + component.toString());
		Fraction f = component.getRoutedDemand();
		lblFlow.setText(HTMLHelper.html(HTMLHelper.fractionVariable("f", f)));
		String nameLF = "l";
		if (component.getOwner().getCurrentEquilibriumType() == EquilibriumType.SYSTEM_OPTIMUM) {
			if (component.getClass() == Arc.class) {
				lblRealLatency.setText(
						HTMLHelper.html(HTMLHelper.fractionVariable(nameLF + "(x)", component.latency(f.div(new Fraction(2))))));
				lblRealLatency.setVisible(true);
			} else {
				lblRealLatency.setVisible(false);
			}
			nameLF += "<sup>(marg)</sup>";
		} else {
			lblRealLatency.setVisible(false);
		}
		lblLatency.setText(HTMLHelper.html(HTMLHelper.fractionVariable(nameLF + "(x)", component.latency(f))));
		lblLatencyFct.setText(HTMLHelper.html(HTMLHelper.function(component.getLatencyFunction(), nameLF)));
		panelLatency.setFunction(component.getLatencyFunction());
		panelLatency.markerAt(f);
		panelLatency.repaint();
		
		lblFlowFct.setText(HTMLHelper.html(HTMLHelper.function(c.getFlowFunction(), "x")));
	}
	
	@Override
	public void validate() {
		super.validate();
		this.select(this.component);
	}
	
}
