package de.warode.wardrop;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import de.warode.wardrop.function.*;

public class MainClass {

	public static void main(String[] args) {
	
		Runnable startGUI = new Runnable() {

			@Override
			public void run() {
				JFrame f = new de.warode.wardrop.gui.MainWindow();
				f.pack();
		        f.setLocationRelativeTo(null);
		        f.setVisible(true);
			}
			
		};
		
		try {
			  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e) {
			  e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(startGUI);
		/*
		Fraction[] a1 = {new Fraction(2), new Fraction(1)};
		Fraction[] b1 = {new Fraction(1), new Fraction(3)};
		Breakpoint[] bp1 = {new Breakpoint(Fraction.ZERO, false), new Breakpoint(new Fraction(2)), new Breakpoint(Fraction.POSITIVE_INFINITY)};
		AffineLinearFunction f = new AffineLinearFunction("f", a1,b1,bp1);
		System.out.println(f.toString());
		
		Fraction[] a2 = {new Fraction(2), new Fraction(4)};
		Fraction[] b2 = {new Fraction(0), new Fraction(-2)};
		Breakpoint[] bp2 = {new Breakpoint(Fraction.ZERO, false), new Breakpoint(new Fraction(1)), new Breakpoint(Fraction.POSITIVE_INFINITY)};
		AffineLinearFunction g = new AffineLinearFunction("g", a2,b2,bp2);
		System.out.println(g.toString());
		
		AffineLinearFunction h = AffineLinearFunction.composition(f, g);
		System.out.println(h.toString());
		*/
	}
}
