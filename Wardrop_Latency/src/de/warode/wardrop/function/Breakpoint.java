package de.warode.wardrop.function;

public class Breakpoint {
	/**
	 * The x-Value of the Breakpoint
	 */
	public final Fraction x;
	/**
	 * true, if this Breakpoint belongs to the interval on the left of p.<br>
	 * This means, if <code>left == true</code> a function using this breakpoint
	 * is continuous <i>from the left</i> in this breakpoint.
	 */
	public final boolean left;
	
	/**
	 * Creates a new breakpoint
	 * @param p the point
	 * @param left if this is set to true, the breakpoint belongs to the left, i.e. the function is continous from the left in this breakpoint.
	 */
	public Breakpoint(Fraction p, boolean left) {
		this.x = p; this.left = left;
	}
	
	/**
	 * Creates a new breakpoint at p. The Breakpoint will belong to the left side except if p == INF
	 * @param p the point
	 */
	public Breakpoint(Fraction p) {
		this(p, !p.isMaxValue());
	}
	
	public String toString() {
		return x.toString();
	}
	
}
