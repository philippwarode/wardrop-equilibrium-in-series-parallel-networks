package de.warode.wardrop.function;

/**
 * Represents a general, piecewise defined, real-valued function
 * @author Philipp
 *
 */
public abstract class Function {
	
	public static final Function zeroFunction = new AffineLinearFunction("ZeroFunction", Fraction.ZERO, Fraction.ZERO);

	public static Function constantFunction(double c) {
		return new AffineLinearFunction("Constant "+c, Fraction.ZERO, Fraction.create(c));
	}
	public static Function constantFunction(double c, Interval domain) {
		return new AffineLinearFunction("Constant "+c, Fraction.ZERO, Fraction.create(c), domain);
	}

	protected Breakpoint[] breakpoints;
	private String name;
	
	protected Function(String name, Breakpoint[] bp) throws IllegalArgumentException {
		if (bp.length < 2) throw new IllegalArgumentException("To few Breakpoints. At least two are needed.");
		for (int i=0; i<bp.length; i++) {
			if (bp[i] == null) {
				throw new NullPointerException("There are null-breakpoint!");
			}
			if (i>0 && bp[i-1].x.compareTo(bp[i].x) > 0) {
				throw new IllegalArgumentException("The breakpoints are not well-ordered!");
			}
		}
		this.name = name;
		this.breakpoints = bp;
	}
	
	protected Function(Breakpoint[]  bp) {
		this("f", bp);
	}
	
	
	protected Function(String name) {
		Breakpoint[] bp = new Breakpoint[2];
		bp[0] = new Breakpoint(Fraction.NEGATIVE_INFINITY);
		bp[1] = new Breakpoint(Fraction.POSITIVE_INFINITY);
		this.breakpoints = bp;
		this.name = name;
	}
	

	protected Function() {
		this("f");
	}
	
		
	/**
	 * Determines the id of the function part in which d is contained. This is
	 * the number i of the breakpoint such that b_i < d < b_i+1
	 * @param x the position x to evaluate
	 * @return the number of the nearest breakpoint left of x
	 * @throws NotInDomainException if x is not in the domain
	 */
	protected int getFunctionPartId(Fraction x) throws NotInDomainException {
		if (!this.getDomain().contains(x)) throw new NotInDomainException(x, this.getDomain());
		for(int i=0; i<this.breakpoints.length - 1; i++) {
			Interval tmpInterval = new Interval(this.breakpoints[i], this.breakpoints[i+1]);
			if (tmpInterval.contains(x)) return i;
		}
		throw new NotInDomainException(x, this.getDomain());
	}
	
	public Interval getDomain() {
		return new Interval(this.breakpoints[0] , this.breakpoints[this.breakpoints.length - 1]);
	}
	
	public String getName() {
		return this.name;
	}
	
	public Fraction evaluate(Fraction x) throws NotInDomainException {
		return this.evaluateFunctionPart(this.getFunctionPartId(x), x);
	}
	
	protected abstract Fraction evaluateFunctionPart(int i, Fraction x);
	
	public boolean isContinous() {
		for (int i=1; i<this.breakpoints.length - 1; i++) {
			Fraction p = this.breakpoints[i].x;
			if (this.evaluateFunctionPart(i-1, p).compareTo((this.evaluateFunctionPart(i, p))) != 0) {
				return false;
			}
		}
		return true;
	}
	
	public String toString() {
		String tmpString = "Function "+this.getName()+"\n";
		tmpString += this.basicInformationString() + "\n";
		
		for (int i=0; i<this.breakpoints.length - 1; i++) {
			tmpString += partToString(i);
		}
		
		return tmpString;
	}
	
	public abstract String partToString(int partID);
	
	public abstract String partToString(int partID, boolean withDomain);

	protected String basicInformationString() {
		String s = "Domain: " + this.getDomain() + "\n";
		s += "Number of (real) breakpoints: " + getNumberOfBreakpoints() + "\n";
		s += this.getName() + " is " + (this.isContinous() ? "" : "not ") + "continous";
		return s;
	}

	/**
	 * Gives the actual number of breakpoints (without the artificial Beginning and End-Point)
	 * @return the number of breakpoints
	 */
	public int getNumberOfBreakpoints() {
		return (this.breakpoints.length - 2);
	}
	
	/**
	 * Returns the i-th breakpoint. The 0th breakpoint is the left bound of the domain.
	 * The (n+2)-nd breakpoint is the right bound of the domain.<br>
	 * Because of these two artificial breakpoints, i can be betweeen 0 and n+2 where n is
	 * the result of <code>this.getNumberOfBreakpoints()</code>.
	 * @param i the index of the breakpoint
	 * @return the breakpoint
	 * @throws ArrayIndexOutOfBoundsException if i<0 or i><code>this.getNumberOfBreakpoints()</code>+2
	 */
	public Breakpoint getBreakpoint(int i) {
		if (i<0 || i>= this.breakpoints.length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return this.breakpoints[i];
	}
	
}
