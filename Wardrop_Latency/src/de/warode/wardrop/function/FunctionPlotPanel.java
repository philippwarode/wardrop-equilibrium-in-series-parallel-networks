package de.warode.wardrop.function;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Iterator;

import javax.swing.JPanel;

public class FunctionPlotPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Function f;
	private Fraction markerPosition = null;
	private Interval domain;
	private Interval range;
	
	public Fraction xStepSize;
	public Fraction yStepSize;
	public Color drawColor;
	
	public FunctionPlotPanel() {
		this(null);
	}
	
	public FunctionPlotPanel(Function f) {
		super();
        setOpaque(false); // we don't paint all our bits
		this.f = f;
	}
	
	public void setFunction(Function f) {
		this.f = f;
		this.domain = this.range = null;
		this.repaint();
	}
	
	public void markerAt (Fraction x) {
		this.markerPosition = x;
	}
	
	@Override
    public Dimension getPreferredSize() {
        Dimension layoutSize = super.getPreferredSize();
        int max = Math.max(layoutSize.width, layoutSize.height);
        return new Dimension(max + 200, max + 200);
    }
	
	@Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        
        Graphics2D g2d = (Graphics2D) g;
        Dimension size = getSize();
        
        if (this.f == null) {
        	g2d.drawString("No function", 0 , size.height / 2);
        	return;
        }
        
        if (this.domain == null || this.range  == null) autoDomainRange();
        Fraction xScale = this.domain.getLength().div(new Fraction(size.width));
        Fraction yScale = this.range.getLength().div(new Fraction(size.height));
        
        //The y-Position (in the Panel) of y = 0
        int y_0 = size.height - Fraction.ZERO.sub(this.range.left).div(yScale).intValue();
        //The x-Position (in the Panel) of x = 0
        int x_0 = Fraction.ZERO.sub(this.domain.left).div(xScale).intValue();
        
        //Plot Axis
        drawAxis(g2d, x_0, y_0, xScale, yScale, size);
        
        if (this.drawColor != null) {
        	g2d.setColor(this.drawColor);
        }
        
        //Plot Function
        if (this.f.getClass() == AffineLinearFunction.class) {
        	plotAffineFunction(g2d, x_0, y_0, xScale, yScale, size);
        } else {
        	plotFunction(g2d, x_0, y_0, xScale, yScale, size);
        }
        
        if (this.markerPosition != null) {
        	g2d.setColor(Color.BLUE);
        	int tmpX = markerPosition.sub(this.domain.left).div(xScale).intValue();
        	Fraction markerY = this.f.evaluate(markerPosition);
        	int tmpY = size.height - markerY.sub(this.range.left).div(yScale).intValue();
        	g2d.fillOval(tmpX-3, tmpY-3, 7, 7);
        	g2d.setColor(Color.BLACK);
        }
    }

	/**
	 * Draws the axis
	 * @param g2d
	 * @param x_0
	 * @param y_0
	 * @param xScale
	 * @param yScale
	 * @param size
	 */
	private void drawAxis(Graphics2D g2d, int x_0, int y_0, Fraction xScale, Fraction yScale, Dimension size) {
		Fraction tickDistance = new Fraction(30);
		//if step sizes are not set, set them automatically
		Fraction xStepSize = this.xStepSize != null ? this.xStepSize :
			xScale.compareTo(tickDistance.inv())>0 ? xScale.mult(tickDistance) : xScale.mult(tickDistance).inv().round().inv();
		Fraction yStepSize = this.yStepSize !=null ? this.yStepSize :
			yScale.compareTo(tickDistance.inv())>0 ? yScale.mult(tickDistance) : yScale.mult(tickDistance).inv().round().inv();
		
			
		//Map axis into the visible area
		y_0 = y_0 < 20 ? 20 : (y_0 > size.height - 20 ? size.height - 20 : y_0);
		x_0 = x_0 < 20 ? 20 : (x_0 > size.width - 20 ? size.width - 20 : x_0);
			
		//Iterator for labels
		Iterator<Fraction> it = null;
		
		//Draw x-axis
        g2d.drawLine(0, y_0, size.width, y_0);
        it = this.domain.iterator(xStepSize);
        //Draw x-labels
        while (it.hasNext()) {
        	Fraction k = it.next();
        	int tmpX = k.sub(this.domain.left).div(xScale).intValue();
        	g2d.drawLine(tmpX, y_0-3, tmpX, y_0+3);
        	String s = k.toString();
        	g2d.drawString(s, tmpX - s.length() * 3, y_0 + 15);
        }
        
        //Draw y-axis
       	g2d.drawLine(x_0, 0, x_0, size.height);
        it = this.range.iterator(yStepSize);
        //Draw y-labels
        while (it.hasNext()) {
        	Fraction k = it.next();
        	int tmpY = size.height - k.sub(this.range.left).div(yScale).intValue();
        	g2d.drawLine(x_0-3, tmpY, x_0+3, tmpY);
        	String s = k.toString();
        	g2d.drawString(s, x_0 - 5 - 6*s.length(), tmpY + 5);
        }
	}
	
	private void plotFunction(Graphics2D g2d, int x_0, int y_0, Fraction xScale, Fraction yScale, Dimension size) {
		for (int x=0; x<=size.width; x++) {
        	Fraction xVal = this.domain.left.add(xScale.mult(new Fraction(x)));
        	Fraction yVal = null;
        	try {
        		yVal = this.f.evaluate(xVal).div(yScale);
            	int y = y_0 - yVal.intValue();
            	if (x<size.width && x>=0 && y<size.height && y>=0) {        	
            		g2d.fillRect(x, y, 1, 1);
            	}
        	} catch (NotInDomainException e) {
        	}
        }
	}
	
	private void plotAffineFunction(Graphics2D g2d, int x_0, int y_0, Fraction xScale, Fraction yScale, Dimension size) {
		Breakpoint bp = this.f.getBreakpoint(0);
		Fraction tmpY = this.f.evaluate(bp.x);
		for (int i=1; i<this.f.breakpoints.length; i++) {
			if (bp.x.compareTo(this.domain.right)>0) {
				break;
			}
			Breakpoint bp2 = this.f.breakpoints[i];
			if (bp2.x.compareTo(this.domain.right)>0) {
				bp2 = new Breakpoint(this.domain.right);
			}
			Fraction tmpY2 = this.f.evaluate(bp2.x);
			int x1 = bp.x.sub(this.domain.left).div(xScale).intValue();
			int x2 = bp2.x.sub(this.domain.left).div(xScale).intValue();
			int y1 = size.height - tmpY.sub(this.range.left).div(yScale).intValue();
			int y2 = size.height - tmpY2.sub(this.range.left).div(yScale).intValue();
			g2d.drawLine(x1, y1, x2, y2);
			bp = bp2;
			tmpY = tmpY2;
		}
	}
	
	private void autoDomainRange() {
		Interval fDomain = f.getDomain();
		if (!fDomain.getLength().isInfinite()) {
			this.domain = fDomain;
		} else if (!fDomain.left.isInfinite()) {
			Fraction tmpR;
			if (f.getNumberOfBreakpoints() > 0) {
				tmpR = f.getBreakpoint(f.getNumberOfBreakpoints()).x;
				Fraction tmpAvgIntervalLength = tmpR.sub(fDomain.left).div(new Fraction(f.getNumberOfBreakpoints()));
				tmpR = tmpR.add(tmpAvgIntervalLength.div(new Fraction(2)));
			} else {
				tmpR = fDomain.left.add(new Fraction(3));
			}
			if (this.markerPosition != null && this.markerPosition.compareTo(tmpR)>0)
				tmpR = markerPosition;
			this.domain = new Interval(fDomain.left,  tmpR);
		} else if (!fDomain.right.isInfinite()) {
			Fraction tmpL;
			if (f.getNumberOfBreakpoints() > 0) {
				tmpL = f.getBreakpoint(1).x;
				Fraction tmpAvgIntervalLength = fDomain.right.sub(tmpL).div(new Fraction(f.getNumberOfBreakpoints()));
				tmpL = tmpL.sub(tmpAvgIntervalLength.div(new Fraction(2)));
			} else {
				tmpL = fDomain.right.sub(new Fraction(3));
			}
			if (this.markerPosition != null && this.markerPosition.compareTo(tmpL)<0)
				tmpL = markerPosition;
			this.domain = new Interval(tmpL, fDomain.right);
		} else {
			//TODO: Adjust interval if domain is infinite in both directions AND function has breakpoints
			this.domain = new Interval(new Fraction(-2), new Fraction(2));
		}
		Fraction half = new Fraction(1,2);
		this.range = new Interval(f.evaluate(this.domain.left).sub(half), f.evaluate(this.domain.right).add(half));
		this.domain = new Interval(this.domain.left.sub(half), this.domain.right.add(half));		
	}
	
}
