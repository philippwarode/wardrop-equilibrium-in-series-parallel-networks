package de.warode.wardrop.function;

public class NotInDomainException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public NotInDomainException(String s) {
		super(s);
	}
	
	public NotInDomainException(Number d, Interval i) {
		super("Domain Exception! "+d.toString()+" is not in "+i.toString());
	}
	
	public NotInDomainException() {
		super();
	}

}
