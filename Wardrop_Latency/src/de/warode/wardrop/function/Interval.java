package de.warode.wardrop.function;

import java.util.Iterator;

public class Interval implements Iterable<Fraction> {

	final Fraction left, right;
	final boolean leftOpen, rightOpen;
	
	public Interval(Fraction l, Fraction r, boolean leftOpen, boolean rightOpen) {
		if (l.compareTo(r)>0) throw new IllegalArgumentException("This interval is degenerated, since "+l+" > "+r);
		this.left = l;
		this.right = r;
		this.leftOpen = leftOpen;
		this.rightOpen = rightOpen;
	}
	
	/**
	 * Creates the CLOSED interval [l, r]
	 * @param l left border
	 * @param r right border
	 */
	public Interval(Fraction l, Fraction r) {
		this(l,r,false, false);
	}
	
	
	public Interval(Breakpoint l, Breakpoint r) {
		this(l.x,r.x,  l.left, !r.left);
	}
	
	public boolean contains(Fraction x) {
		if (!leftOpen && !rightOpen) {
			return (left.compareTo(x) <=0 && right.compareTo(x) >= 0);
		}
		if (leftOpen && !rightOpen) {
			return (left.compareTo(x) < 0 && right.compareTo(x) >= 0);
		}
		if (!leftOpen && rightOpen) {
			return (left.compareTo(x) <= 0 && right.compareTo(x) > 0);
		}
		return (left.compareTo(x) < 0 && right.compareTo(x) > 0);
	}
	
	@Override
	public String toString() {
		if (!leftOpen && !rightOpen) {
			return "["+left+", "+right+"]";
		}
		if (leftOpen && !rightOpen) {
			return "("+left+", "+right+"]";
		}
		if (!leftOpen && rightOpen) {
			return "["+left+", "+right+")";
		}
		return "("+left+", "+right+")";
	}
	
	public boolean equals(Interval interval) {
		return (this.left.equals(interval.left) && this.right.equals(interval.right) &&
				this.rightOpen == interval.rightOpen && this.leftOpen == interval.leftOpen);
	}
	
	Breakpoint[] toBreakpoints() {
		Breakpoint[] bp = new Breakpoint[2];
		bp[0] = new Breakpoint(this.left, this.leftOpen);
		bp[1] = new Breakpoint(this.right, !this.rightOpen); 
		return bp;
	}
	
	public Fraction getLength() {
		if (this.left.isInfinite() || this.right.isInfinite())
			return Fraction.POSITIVE_INFINITY;
		return this.right.sub(this.left);
	}

	@Override
	public Iterator<Fraction> iterator() {
		return new IntervalIterator();
	}
	
	public Iterator<Fraction> iterator(Fraction stepSize) {
		return new IntervalIterator(stepSize);
	}
	
	public Iterator<Fraction> iterator(Fraction stepSize, Fraction anchor) {
		return new IntervalIterator(stepSize, anchor);
	}
	
	private class IntervalIterator implements Iterator<Fraction> {

		private Fraction stepSize;
		private Fraction position;
		/**
		 * Creates an iterator that iterates to all multiples of <code>stepSize</code>
		 * such that one of the multiples is <code>anchor</code>
		 * @param stepSize the stepSize
		 * @param anchor an Anchor-Point in the interval
		 */
		private IntervalIterator(Fraction stepSize, Fraction anchor) {
			init(stepSize, anchor);
		}
		
		private void init(Fraction stepSize, Fraction anchor) {
			this.stepSize = stepSize;
			Fraction steps = anchor.sub(left).div(stepSize).floor();
			this.position = anchor.sub(stepSize.mult(steps));
		}
		
		private IntervalIterator(Fraction stepSize) {
			Fraction tmpAnchor = Interval.this.left.nextInteger();
			if (!Interval.this.contains(tmpAnchor)) {
				tmpAnchor = Interval.this.left.nextFraction(stepSize);
				if (!Interval.this.contains(tmpAnchor)) {
					tmpAnchor = Interval.this.right.sub(Interval.this.left).div(new Fraction(2));
				}
			}
			init(stepSize, tmpAnchor);
		}
		
		private IntervalIterator() {
			this(new Fraction(1));
		}
		
		@Override
		public boolean hasNext() {
			return Interval.this.contains(position);
		}

		@Override
		public Fraction next() {
			Fraction tmp = this.position;
			position = position.add(stepSize);
			return tmp;
		}
		
		public String toString() {
			String s = "Interval Iterator of "+Interval.this.toString()+" with stepsize "+stepSize+"\n";
			Fraction tmp = position;
			s+="Remaining:";
			if (!Interval.this.contains(position)) {
				s += " none";
			} else {
				while (Interval.this.contains(tmp)) {
					s +=" " + tmp.toString() ;
					tmp = tmp.add(stepSize);
				}
			}
			return s;
		}
		
	}
	
}
