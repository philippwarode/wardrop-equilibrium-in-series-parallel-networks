package de.warode.wardrop.function;

import java.text.ParseException;

/**
 * A Fraction class. A fraction consists of a numerator p and a denominator q. 
 * Supports basic arithmetic operations (see Interface BasicArithmetics) and
 * extends Number.
 * 
 * @author Philipp Warode
 *
 */
public class Fraction extends Number implements Comparable<Fraction>, BasicArithmetics<Fraction> {

	public static final Fraction NEGATIVE_INFINITY = new Fraction(0,1,true, true);
	public static final Fraction POSITIVE_INFINITY = new Fraction(0,1,false, true);
	public static final Fraction ZERO = create(0);
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * The numerator
	 */
	private final long p;
	/**
	 * The denominator
	 */
	private final long q;
	/**
	 * true, if this number is negative
	 */
	private final boolean negative;
	/**
	 * true, if this number is infinite
	 */
	private final boolean infinity;
		
	/**
	 * Create a new Fraction numerator/denominator
	 * @param numerator the numerator
	 * @param denomiator the denominator
	 */
	public Fraction(long numerator, long denomiator) {
		this(numerator, denomiator, ((numerator < 0 ) != (denomiator < 0)), false, false);
	}
	
	/**
	 * Creates a new Fraction from the integer <code>number</code>.
	 * @param number
	 */
	public Fraction(int number) {
		this(number, 1, (number<0), false, true);
	}
	

	/**
	 * Creates a new Fraction from the long <code>number</code>.
	 * @param number
	 */
	public Fraction(long number) {
		this(number, 1, (number<0), false, true);
	}
	
	/**
	 * Creates a new fraction.<br>
	 * The signs in numerator / denominator will be ignored, only the field negative is important.
	 * @param numerator the numerator
	 * @param denominator the denominator
	 * @param negative specifies if this is a negative fraction
	 * @param infinity specifies if this is infinity (makes numerator, denominator obsolete)
	 */
	private Fraction(long numerator, long denominator, boolean negative, boolean infinity) {
		this(numerator, denominator, negative, infinity, false);
	}
	
	/**
	 * Creates a new fraction.<br>
	 * The signs in numerator / denominator will be ignored, only the field negative is important.
	 * @param numerator the numerator
	 * @param denominator the denominator
	 * @param negative specifies if this is a negative fraction
	 * @param infinity specifies if this is infinity (makes numerator, denominator obsolete)
	 * @param simplified specifies if the given numerator/denominator are already simplified
	 */
	private Fraction(long numerator, long denominator, boolean negative, boolean infinity, boolean simplified) {
		if (denominator == 0 && !infinity) {
			throw new ArithmeticException("Division by zero");
		}
		numerator = Math.abs(numerator);
		denominator = Math.abs(denominator);
		//Simplify the Fraction if necessary
		if (!simplified) {
			long gcd = gcd(numerator, denominator);
			numerator = numerator / gcd;
			denominator = denominator / gcd;
		}
		this.p = numerator;
		this.q = denominator;
		this.negative = negative;
		this.infinity = infinity;
	}
	
	public long getNumerator() {
		if (this.infinity) {
			return 1;
		}
		return this.p;
	}
	
	public long getDenominator() {
		if (this.infinity) {
			return 0;
		}
		return this.q;
	}

	@Override
	public Fraction neg() {
		return new Fraction(p, q, !negative, infinity, true);
	}
	
	/**
	 * Returns the absolute value of this fraction
	 * @return the absolute value
	 */
	public Fraction abs() {
		if (!this.negative)
			 return this;
		
		return this.neg();
	}

	/**
	 * Returns a new Fraction with the value <code>this</code> + <code>number</code>.<br>
	 * <code>number == null</code> will be interpreted as if <code>number == Fraction.ZERO</code>
	 * @throws ArithmeticException if INF - INF
	 */
	@Override
	public Fraction add(Fraction number) throws ArithmeticException {
		//Interpret null as zero
		if (number == null) {
			return this;
		}
		
		//Exception if -INF + INF
		if (this.infinity && number.infinity && (this.negative != number.negative)) {
			throw new ArithmeticException("-INFINITY + INFINITY not well defined");
		}
		//Return +/- INF if one of the summands is INF 
		if (this.infinity || number.infinity) {
			if ((this.negative && this.infinity) || (number.negative && number.infinity)) {
				return Fraction.NEGATIVE_INFINITY;
			}
			return Fraction.POSITIVE_INFINITY;
		}
		long newQ = this.q * number.q;
		//Same sign, so just add the numbers
		if (this.negative == number.negative) {
			return new Fraction(p * number.q + q * number.p, newQ, this.negative, false);
		}
		//Different signs
		long newP;
		if (this.negative) {
			newP = - p * number.q + q * number.p;
		} else {
			newP = p * number.q - q * number.p;
		}
		return new Fraction(newP, newQ);
	}

	/**
	 * Returns a new Fraction with the value <code>this</code> - <code>number</code>.<br>
	 * <code>number == null</code> will be interpreted as if <code>number == Fraction.ZERO</code>
	 * @throws ArithmeticException if INF - INF
	 */
	@Override
	public Fraction sub(Fraction number) {
		if (number == null) {
			return this;
		}
		return this.add(number.neg());
	}

	@Override
	public Fraction inv() {
		if (this.infinity) {
			throw new ArithmeticException("Division by infinity");
		}
		if (this.isZero()) {
			throw new ArithmeticException("Division by zero");
		}
		return new Fraction(this.q, this.p, this.negative, false, true);
	}

	@Override
	public Fraction mult(Fraction number) {
		if (number == null) {
			return Fraction.ZERO;
		}
		return new Fraction(this.p * number.p, this.q * number.q, this.negative != number.negative, this.infinity || number.infinity);
	}

	@Override
	public Fraction div(Fraction number) throws ArithmeticException {
		if (number == null) {
			throw new ArithmeticException("Division by zero. In particular the divisor was null");
		}
		return this.mult(number.inv());
	}
	
	public Fraction pow(Fraction exponent) {
		if (!exponent.isInteger()) {
			throw new ArithmeticException("Only integer exponents are allowed.");
		}
		int exp = exponent.intValue();
		return pow(exp);
	}
	
	@Override
	public Fraction pow(int exp) {
		long newNum = 1;
		long newDen = 1;
		boolean negExp = (exp < 0);
		if (negExp) {
			exp = - exp;
		}
		
		try {
			for (int i=0; i<exp; i++) {
				newNum = Math.multiplyExact(newNum, this.p);
				newDen = Math.multiplyExact(newDen, this.q);
			}
		} catch (ArithmeticException e) {
			if (exp % 2 != 0 && this.negative) {
				return Fraction.NEGATIVE_INFINITY;
			} else {
				return Fraction.POSITIVE_INFINITY;
			}
		}
		Fraction result = new Fraction(newNum, newDen,  this.negative && exp % 2 != 0, false, true);
		if (negExp)
			return result.inv();
		
		return result;
	}

	@Override
	public boolean isZero() {
		return (this.p == 0);
	}

	@Override
	public boolean isOne() {
		return (this.p == this.q);
	}

	/**
	 * Returns true, if this is +INF
	 */
	@Override
	public boolean isMaxValue() {
		return (this.infinity && !this.negative);
	}

	/**
	 * Returns true, if this is -INF
	 */
	@Override
	public boolean isMinValue() {
		return (this.infinity && this.negative);
	}

	@Override
	public int compareTo(Fraction o) {
		if (o==null) {
			o=Fraction.ZERO;
		}
		
		//Handle trivial cases fast
		if (this.negative && !o.negative) {
			return -1;
		}
		if (!this.negative && o.negative) {
			return 1;
		}
		
		//Both fractions are the same (uses the assertion, that both fractions are always simplified)
		if ((this.infinity && o.infinity) || (!this.infinity && !o.infinity && this.q == o.q && this.p == o.p)) {
			return 0;
		}
		
		boolean thisHasBiggerAbs = !o.infinity && ((this.p * o.q > o.p * this.q) || this.infinity);
		if (this.negative && thisHasBiggerAbs || !this.negative && !thisHasBiggerAbs) {
			return -1;
		}
		return 1;
	}
	
	/**
	 * Returns true, if this is the same Fraction as <code>f</code>.<br>
	 * Two fractions are the same if they have the same numerator and denominator after they are simplified
	 * @param f the other Fraction to compare with
	 * @return true if this is the same Fraction as f
	 */
	public boolean equals(Fraction f) {
		if (f==null) {
			f = Fraction.ZERO;
		}
		if (this.negative == f.negative && this.infinity == f.infinity && this.p == f.p && this.q == f.q) {
			return true;
		}
		return false;
	}
	
	/**
	 * Creates a new Fraction with value <code>i</code>
	 * @param i the Integer Value
	 * @return a new Fraction
	 */
	public static Fraction create(int i) {
		boolean neg = (i<0);
		if (neg) i = -i;
		return new Fraction(i,1,neg,false,true);
	}
	
	/**
	 * Creates a new Fraction with value <code>l</code>
	 * @param l the Long Value
	 * @return a new Fraction
	 */
	public static Fraction create(long l) {
		boolean neg = (l<0);
		if (neg) l = -l;
		return new Fraction(l,1,neg,false,true);
	}
	
	/**
	 * Creates a new Fraction with value <code>d</code>.<br>
	 * Precision is 10 decimals.
	 * @param d the Long Value
	 * @return a new Fraction
	 */
	public static Fraction create(double d) {
		boolean neg = d<0;
		if (d<0) d= d*-1;
		long n = Math.round(d);
		d = (d - n) * 10000000000L;
		long dec = Math.round(d);
		return new Fraction(n,1L,neg, false, true).add(new Fraction(dec, 10000000000L, neg, false));
	}

	@Override
	public int intValue() {
		long res = p / q;
		if (res > Integer.MAX_VALUE) res = Integer.MAX_VALUE;
		if (this.negative) return - (int) res;
		return (int) res;
	}

	@Override
	public long longValue() {
		if (this.negative) return -p/q;
		return p/q;
	}

	@Override
	public float floatValue() {
		if (this.negative) return -p/q;
		return p/q;
	}

	@Override
	public double doubleValue() {
		if (this.negative) return -p/q;
		return p/q;
	}
	
	@Override
	public String toString() {
		String sign = this.negative ? "-" : "";
		if (this.infinity) {
			return sign + "INF";
		}
		if (q == 1 || p == 0) {
			return sign + Long.toString(p);
		}
		return sign + p + "/" + q;
	}
	
	/**
	 * Returns true if this is an integer, i.e. if the denominator is 1.
	 * @return true if this is an integer
	 */
	public boolean isInteger() {
		return this.q == 1;
	}
	
	/**
	 * Returns the next integer <code>i</code>.<br>
	 * This is the smallest integer <code>i</code> that satisfies <code>i&gt;=this</code>.<br>
	 * In particular, if <code>this.isInteger() == true</code> this returns <b>this</b> 
	 * @return the next integer
	 */
	public Fraction nextInteger() {
		if (this.isInteger()) return this;
		if (this.negative) {
			return new Fraction(this.longValue()); 
		}
		return new Fraction(this.longValue() + 1);
	}
	
	/**
	 * Returns the next Fraction <code>f</code> that is a multiple of <code>x</code>.<br>
	 * This is the smallest fraction <code>f</code> that satisfies <code>f = k * x</code> for
	 * a integer <code>k</code> and <code>f&gt;=this</code>.<br>
	 * In particular, if this is an multiple of x this returns <b>this</b> 
	 * @param x the Fraction to be searched for
	 * @return the next Fraction that is a multiple of x
	 */
	public Fraction nextFraction(Fraction x) {
		if (this.isInteger()) return this;
		Fraction tmp = this.div(x);
		long val;
		if (this.negative) {
			val = tmp.longValue();
		} else {
			val = tmp.longValue() + 1;
		}
		
		return x.mult(new Fraction(val));
		
	}
	
	/**
	 * Calculates recursively the greatest common division of a and b.<br>
	 * Used for simplification.
	 * @param a first integer
	 * @param b second integer
	 * @return the greatest common divisor
	 */
	private static long gcd(long a, long b) {
		if (b == 0) return a;
		return gcd(b, a % b);
	}
	
	public String toStringDEBUG() {
		String s = "FRACTION ";
		s += this.toString();
		s += "\n"+"p: "+p;
		s += "\n"+"q: "+q;
		s += "\n"+"neg: "+this.negative;
		s += "\n"+"inf: "+this.infinity;
		return s;
	}

	/**
	 * Rounds this fraction to an integer.<br>
	 * Returns this.nextInteger() if this >= xx.5 and this.nextInteger() - 1 if this < xx.5
	 * @return a Fraction with the rounded value
	 */
	public Fraction round() {
		if (this.isInteger()) return this;
		Fraction nextInt = this.nextInteger();
		if (nextInt.sub(this).compareTo(new Fraction(1,2))<=0) {
			return nextInt;
		}
		return new Fraction(nextInt.p - 1, 1);
	}
	
	public Fraction floor() {
		if (this.isInteger()) return this;
		Fraction tmp = this.abs().nextInteger();
		tmp = new Fraction(tmp.p - 1, tmp.q, this.negative, false, false);
		return tmp;
	}
	
	public Fraction ceil() {
		return this.nextInteger();
	}

	/**
	 * 
	 * @return true if this number is infinite
	 */
	public boolean isInfinite() {
		return this.infinity;
	}
	
	/**
	 * Returns the maximum of the Fractions a and b.
	 * @param a first fraction
	 * @param b second fraction
	 * @return the maximum of a and b
	 */
	public static Fraction max(Fraction a, Fraction b) {
		return a.compareTo(b) >= 0 ? a : b;
	}
	
	/**
	 * Returns the minimum of the Fractions a and b.
	 * @param a first fraction
	 * @param b second fraction
	 * @return the minimum of a and b
	 */
	public static Fraction min(Fraction a, Fraction b) {
		return a.compareTo(b) <= 0 ? a : b;
	}

	public static Fraction parse(String s) throws ParseException {
		String[] parts = s.split("/");
		if (parts.length < 2) {
			try {
				return create(Double.parseDouble(s));
			} catch (Exception e) {
				throw new ParseException("Couldn't parse Fraction from "+s,0);
			}
		}
		long n, d;
		try {
			n = Long.parseLong(parts[0]);
			d = Long.parseLong(parts[1]);
		} catch (NumberFormatException e) {
			throw new ParseException("Couldn't parse Fraction from "+s, 0);
		}
		return new Fraction(n,d);
	}

}
