package de.warode.wardrop.function;

public interface BasicArithmetics<T> {

	/**
	 * Returns the negative of this number
	 * @return -this
	 */
	public abstract T neg();
	
	/**
	 * Returns a new number of type T with value this + number
	 * @param number the other number
	 * @return this + number
	 */
	public abstract T add(T number);
	
	/**
	 * Returns a new number of type T with value this - number
	 * @param number the other number
	 * @return this - number
	 */
	public T sub(T number);
				
	/**
	 * Returns the reciprocal of this number
	 * @return 1/this
	 */
	public abstract T inv();
	
	/**
	 * Returns a new number of type T with value this * number
	 * @param number
	 * @return this * number
	 */
	public abstract T mult(T number);
	
	/**
	 * Returns a new number of type T with value this / number
	 * @param number
	 * @return this / number
	 * @throws ArithmeticException if number is equal to zero
	 */
	public T div(T number) throws ArithmeticException;
	
	/**
	 * Returns the power this number with exponent exp. Throws an exception if the result
	 * is not an element of type T, e.g. if one calculates a negative power of a natural number.
	 * @param exp the exponent 
	 * @return the power this^exponent
	 * @throws ArithmeticException if the result is not an element of type T
	 */
	public T pow(int exp) throws ArithmeticException;
	
	/**
	 * Returns true if this == 0, i.e. if this is the neutral element of addition
	 * @return true if this is the neutral element of addition
	 */
	public abstract boolean isZero();
	
	/**
	 * Returns true if this == 1, i.e. if this is the neutral element of multiplication
	 * @return true if this is the neutral element of multiplication
	 */
	public abstract boolean isOne();
	
	/**
	 * Returns true if this is the MaximalValue of the number type T
	 * @return true if this is the MaximalValue of the number type T
	 */
	public abstract boolean isMaxValue();
	
	/**
	 * Returns true if this is the MaximalValue of the number type T
	 * @return true if this is the MaximalValue of the number type T
	 */
	public abstract boolean isMinValue();
	
}
