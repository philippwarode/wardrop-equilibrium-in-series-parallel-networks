package de.warode.wardrop.function;

public class PolynomialFunction extends Function {
	
	protected Fraction[][] a;
	private int degree;
	
	public PolynomialFunction(String name, Fraction[][] coeff, Breakpoint[] bp) {
		super(name, bp);
		//SanityCheck
		this.degree = coeff.length - 1;
		this.a = coeff;
		if (!coeffSanityCheck())
			throw new IllegalArgumentException("Coefficient array sizes do not match!");
	}
	
	public PolynomialFunction(String name, Fraction[][] coeff) {
		super(name);
		this.degree = coeff.length - 1;
		this.a = coeff;
		if (!coeffSanityCheck())
			throw new IllegalArgumentException("Coefficient array sizes do not match!");
	}
	
	private boolean coeffSanityCheck() {
		if (this.degree < 0) throw new IllegalArgumentException("Not enough coefficients");
		for (int i=0; i<this.degree; i++) {
			if (a[i].length != a[i+1].length)
				return false;
		}
		return true;
	}
	
	public Fraction getCoefficient(int i, int part) {
		return a[i][part];
	}

	@Override
	protected Fraction evaluateFunctionPart(int i, Fraction x) {
		Fraction result = Fraction.ZERO;
		for (int j=0; j<=degree; j++) {
			result = result.add(x.pow(j).mult(a[j][i]));
		}
		return result;
	}

	@Override
	public String partToString(int partID) throws ArrayIndexOutOfBoundsException {
		return partToString(partID, true);
	}
	
	@Override
	public String partToString(int partID, boolean withDomain) throws ArrayIndexOutOfBoundsException {
		if (partID < 0 || partID > this.breakpoints.length - 2) 
			throw new ArrayIndexOutOfBoundsException(partID);
		
		String tmpString ="";
		boolean first = true;
		for (int i=this.degree; i>=0; i--) {
			if (!a[i][partID].equals(Fraction.ZERO)) {
				if (a[i][partID].compareTo(Fraction.ZERO)<0) {
					tmpString += "- "; 
				} else if (!first) {
					tmpString += "+ ";
				}
				tmpString += a[i][partID].abs().toString()+" ";
				if (i==1) {
					tmpString += "x"; 
				} else if (i>1) {
					tmpString += "x^"+i;
				}
				first = false;
			}
		}
		if (tmpString.equals("")) tmpString = "0";

		tmpString += "\n";
		
		if (withDomain)
			tmpString += "\t for x in "+new Interval(this.breakpoints[partID], this.breakpoints[partID+1]).toString();
		
		tmpString += "\n";
		return tmpString;
	}
	
	public Function neg() {
		Fraction[][] c = new Fraction[this.a.length][this.breakpoints.length-1];
		for (int i=0; i<this.a.length; i++) {
			for (int j=0; j<this.breakpoints.length-1; j++) {
				c[i][j] = a[i][j].neg();
			}
		}
		
		if (this.degree == 1) {
			return new AffineLinearFunction("-"+this.getName(), c[1], c[0], this.breakpoints);
		}
		
		return new PolynomialFunction("-"+this.getName(),c, this.breakpoints);
	}

}
