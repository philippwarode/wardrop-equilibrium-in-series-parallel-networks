package de.warode.wardrop.function;

public class AffineLinearFunction extends PolynomialFunction {
	
	/*
	private Fraction[] a;
	private Fraction[] b;
	*/
	
	private static Fraction[][] makeCoefficients(Fraction[] a, Fraction[] b) {
		Fraction[][] c = {b, a};
		return c;
	}
	
	private static Fraction[][] makeCoefficients(Fraction a, Fraction b) {
		Fraction[][] c = {{b}, {a}};
		return c;
	}
	
	public AffineLinearFunction(String name, Fraction[] a, Fraction[] b, Breakpoint[] bp)  throws IllegalArgumentException{
		super(name, makeCoefficients(a, b), bp);
		if (!(a.length == b.length && b.length + 1 == bp.length)) {
			throw new IllegalArgumentException("Array sizes do not match!");
		}
	}
	
	public AffineLinearFunction(Fraction[] a, Fraction[] b, Breakpoint[] bp)  throws IllegalArgumentException {
		this("f",a,b,bp);
	}
	
	public AffineLinearFunction(String name, Fraction a, Fraction b, Interval domain) {
		super(name, makeCoefficients(a,b), domain.toBreakpoints());
	}
	
	public AffineLinearFunction(String name, long a, long b, Interval domain) {
		this(name, new Fraction(a), new Fraction(b), domain);
	}
	
	public AffineLinearFunction(Fraction a, Fraction b, Interval domain) {
		this("f", a,b, domain);
	}
	
	public AffineLinearFunction(long a, long b, Interval domain) {
		this("f", a,b, domain);
	}
	
	
	public AffineLinearFunction(String name, Fraction a, Fraction b) {
		super(name, makeCoefficients(a, b));
	}
	
	public AffineLinearFunction(String name, long a, long b) {
		this(name, new Fraction(a), new Fraction(b));
	}
	
	
	public AffineLinearFunction(Fraction a, Fraction b) {
		this("f", a,b);
	}
	
	public AffineLinearFunction(long a, long b) {
		this("f", a,b);
	}
	

	public Fraction getSlopeAt(Fraction x) {
		return getSlope(this.getFunctionPartId(x));
	}
	
	public Fraction getSlope(int i) {
		return this.getCoefficient(1, i);
	}
	
	public Fraction getConstantAt(Fraction x) {
		return getConstant(this.getFunctionPartId(x));
	}
	
	public Fraction getConstant(int i) {
		return this.getCoefficient(0, i);
	}
		
	public static AffineLinearFunction sum(AffineLinearFunction f, AffineLinearFunction g, String newName) throws NotInDomainException {
		if (!f.getDomain().equals(g.getDomain())) 
			throw new NotInDomainException("Domains of "+f.getName() + " and " + g.getName()+ " don't coincide! " 
					+ f.getDomain() + " =/= " + g.getDomain());
		int numBP = f.getNumberOfBreakpoints() + g.getNumberOfBreakpoints();
		Fraction[] a = new Fraction[numBP+1];
		Fraction[] b = new Fraction[numBP+1];
		Breakpoint[] bp = new Breakpoint[numBP+2];
		bp[0] = new Breakpoint(f.getDomain().left, f.getDomain().leftOpen);
		
		//Iterate through all Breakpoints
		
		//Current breakpoint of f (i) or g (j) or the sum (k)
		int i = 0;
		int j = 0;
		int k = 0;
		
		while (i<f.getNumberOfBreakpoints() + 1 || j < g.getNumberOfBreakpoints() +1 ) {
			k++;
			a[k-1] = f.a[1][i].add(g.a[1][j]);
			b[k-1] = f.a[0][i].add(g.a[0][j]);
			//Select next breakpoint
			if (f.breakpoints[i+1].x == g.breakpoints[j+1].x) {
				//Breakpoints are at the same x, we take the breakpoint that has the open side (belongs not to the left) first
				if (!f.breakpoints[i+1].left && g.breakpoints[j+1].left) {
					bp[k] = f.breakpoints[i+1];
					i++;
				} else if (f.breakpoints[i+1].left && !g.breakpoints[j+1].left) {
					bp[k] = g.breakpoints[j+1];
					j++;
				} else {
					//Breakpoints actually coincide, we can save one breakpoint
					bp[k] = f.breakpoints[i+1];
					i++; j++;
				}
			} else if (f.breakpoints[i+1].x.compareTo(g.breakpoints[j+1].x) < 0) {
				//Breakpoint of f comes next
				bp[k] = f.breakpoints[i+1];
				i++;
			} else {
				//Breakpoint of g comes next
				bp[k] = g.breakpoints[j+1];
				j++;
			}
		}
		
		bp[k] = new Breakpoint(f.getDomain().right, !f.getDomain().rightOpen);
		
		//if all breakpoints were create, create new function
		if (k == numBP +1) {
			return new AffineLinearFunction(newName, a, b, bp);
		}
		
		//if k < numBP (i.e. some breakpoints coincided), shorten all arrays
		Fraction[] shortA = new Fraction[k];
		Fraction[] shortB = new Fraction[k];
		Breakpoint[] shortBP = new Breakpoint[k+1];
		for (int it=0; it<k; it++) {
			shortA[it] = a[it];
			shortB[it] = b[it];
			shortBP[it] = bp[it];
		}
		shortBP[k] = new Breakpoint(f.getDomain().right, !f.getDomain().rightOpen);
		
		return new AffineLinearFunction(newName, shortA, shortB, shortBP);
				
	}
	
	public static AffineLinearFunction sum(AffineLinearFunction f, AffineLinearFunction g) {
		return sum(f,g, "(" + f.getName() + " + " + g.getName() + ")");
	}
	
	public static AffineLinearFunction composition(AffineLinearFunction f, AffineLinearFunction g) throws NotInDomainException {
		int numBP = (f.getNumberOfBreakpoints()+1) * (g.getNumberOfBreakpoints()+1);
		Fraction[] a = new Fraction[numBP];
		Fraction[] b = new Fraction[numBP];
		Breakpoint[] bp = new Breakpoint[numBP+1];
		bp[0] = new Breakpoint(g.getDomain().left, g.getDomain().leftOpen);
		
		//Function part counter
		int k=0;
		
		//Current function part of g
		int i=0;
		//Current function part of f
		int j=-1;

		while (true) {
			//get the value of g at the left breakpoint
			Fraction valOfGLeft = g.evaluateFunctionPart(i, bp[k].x);
			//get the function part of f, such that valOfGLeft is in the domain of this function part
			//if the function part was not determined before (<=> j==-1)
			if (j==-1) j = f.getFunctionPartId(valOfGLeft);
			//Compute the actual function
			//System.out.println("k="+k+"\n bp="+bp[k].x+"\n i="+i+" j="+j);
			
			a[k] = f.getSlope(j).mult(g.getSlope(i));
			b[k] = f.getConstant(j).add(f.getSlope(j).mult(g.getConstant(i)));
			
			//System.out.println(" a="+a[k]+"\n b="+b[k]);
			
			//Find the next breakpoint
			Fraction nextBPofF;
			int nextJ;
			//Compute the next x s.t. a breakpoint of f is reached
			//(either the next bp if the slope of g is positive or the last if it is negative)
			if (g.getSlope(i).isZero()) {
				nextBPofF = Fraction.POSITIVE_INFINITY;
				nextJ = j;
			} else {
				if (g.getSlope(i).compareTo(Fraction.ZERO)>0) {
					nextJ = j+1;
				} else {
					nextJ = j-1;
				}
				nextBPofF = f.breakpoints[nextJ].x.sub(g.getConstant(i)).div(g.getSlope(i));
			}
			
			Fraction nextBP = g.breakpoints[i+1].x;
			//System.out.println("bp_f = "+nextBPofF+"  bp_g = "+nextBP+"  nextJ="+nextJ);
			if (nextBPofF.compareTo(nextBP)<0) {
				nextBP = nextBPofF;
				j=nextJ;
			} else {
				i=i+1;
				j=-1;
			}
			
			k=k+1;
			bp[k] = new Breakpoint(nextBP);
			
			//System.out.println("New BP="+bp[k].x);
			
			//If the end of the domain of g is reached, break
			if (nextBP.compareTo(g.getDomain().right)>=0) {
				break;
			}
		}
		
		//Shorten Arrays
		Fraction[] sA = new Fraction[k];
		Fraction[] sB = new Fraction[k];
		Breakpoint[] sBP = new Breakpoint[k+1];
		
		sBP[0] = bp[0];
		for (int n=0; n<k; n++) {
			sA[n] = a[n];
			sB[n] = b[n];
			sBP[n+1] = bp[n+1]; 
		}
		return new AffineLinearFunction("(" + f.getName() + ") o (" +g.getName()+")",sA, sB, sBP);
		
	}
	
	public AffineLinearFunction compose(AffineLinearFunction g) {
		return composition(this, g);
	}

}
