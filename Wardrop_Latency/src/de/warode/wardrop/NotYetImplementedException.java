package de.warode.wardrop;

public class NotYetImplementedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NotYetImplementedException() {
		super();
	}
	
	public NotYetImplementedException(String s) {
		super(s);
	}

}
